# pix

Pix is a package loader that can load pkx packages hosted on GitLab by specifying a selector, which can be a string containing a url to the GitLab repository of the package, a string containing one of the default muvi packages (like 'echo') or an object with targetting abilities.

## Usage

You can use the hosted pix bootloader in a browser at `https://muvi.gitlab.io/pix/`. You can also install pix from npm by performing `npm install -g muvi`.

### Shell

```
pix [options] <selector>
```

#### Examples

```
pix --config "{ \"text\" : \"hello world\" }" https://gitlab.com/muvi/echo
```

Since echo is part of the muvi repository you can ommit the url and just specify the package name like this:

```
pix --config "{ \"text\" : \"hello world\" }" echo
```

### URI

```
https://muvi.gitlab.io/pix/?<selector>&[option]=[value]&[option]=[value]&...
```

#### Examples

```
https://muvi.gitlab.io/pix/?https://gitlab.com/muvi/echo&--config={"text":"hello world"}
```

You may need to encode the entiry json parameter to be a legal url. However, most browsers can handle the url above. In the folowing example you can see that the json parameter is encoded. Also, because echo is in the default muvi repository, we do not need to specify the full url in the package selector parameter.

```
https://muvi.gitlab.io/pix/?echo&--config=%7B%22text%22%3A%22hello%20world%22%7D
```

## Embedding

### Browser

When embedding pix in a browser app, include the following script tag. The example assumes you have extracted the pix repository to a subdirectory in your project called 'pix'. When you download the pix repository you still need to initialise some submdoules that pix uses by executing 'git submodule update --remote --init' to make it work.
```html
<script src="./pix/pix.js"></script>
```

### node.js

When embedding from node, use the following code.
```javascript
var pix = require("pix");
```

### Loading Packages

Then use javascript to load packages.
```javascript
pix({
    "selector" : "echo",
    "config" : { 
        "text" : "hello world"
    }
}).then(function(module) {
    console.log(  "[pix] Successfully loaded '" + module.id + "'.");
}).catch(function(e) {
    console.error("[pix] ", e);
});
```
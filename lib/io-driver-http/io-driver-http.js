/////////////////////////////////////////////////////////////////////////////////////
//
// module 'io-driver-http.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "io-driver-http.1.0.0/";
    define.parameters.pkx = {
        "name": "io-driver-http",
        "version": "1.0.0",
        "main": "io-driver-http.js",
        "dependencies": {
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "type": "https://gitlab.com/muvi/type#semver:^1.0",
            "string-validation": "https://gitlab.com/muvi/string-validation#semver:^1.0",
            "event": "https://gitlab.com/muvi/event#semver:^1.0",
            "host": "https://gitlab.com/muvi/host#semver:^1.0",
            "io-access": "https://gitlab.com/muvi/io-access#semver:^1.0",
            "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
            "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "type": "https://gitlab.com/muvi/type#semver:^1.0",
                "string-validation": "https://gitlab.com/muvi/string-validation#semver:^1.0",
                "event": "https://gitlab.com/muvi/event#semver:^1.0",
                "host": "https://gitlab.com/muvi/host#semver:^1.0",
                "io-access": "https://gitlab.com/muvi/io-access#semver:^1.0",
                "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
                "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0"
            },
            "main": "io-driver-http.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.parameters.dependencies.push(define.cache.get("string-validation.1.0/"));
    define.parameters.dependencies.push(define.cache.get("event.1.0/"));
    define.parameters.dependencies.push(define.cache.get("host.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-access.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-uri.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-stream.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // io-driver-http
    //
    //    IO module that implements HTTP & HTTPS protocol support.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
        Error =    require("error");
    var type =     require("type");
    var event =    require("event");
    var string =   require("string-validation");
    var ioURI =    require("io-uri");
    var ioAccess = require("io-access");
    var ioStream = require("io-stream");
    var host =     require("host");
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // StreamHTTP Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function StreamHTTP(uri, access) {
        var own = this;
    
        var written = false;
        var closed = false;
    
        var buffer = null;
        var acceptRanges = null;
    
        if (access && access != ioAccess.READ) {
            throw new Error(ioAccess.ERROR_ACCESS_DENIED, "StreamHTTP does not support '" + access + "'.");
        }
    
        this.headers = {};
    
        this.getAccess = function() {
            return new Promise(function(resolve, refuse) { resolve(ioAccess.READ); });
        };
        this.getName = function() {
            var name = uri.toString();
            var idxLastSlash = name.lastIndexOf("/");
            return idxLastSlash == name.length - 1? "" : (idxLastSlash >= 0? name.substr(idxLastSlash + 1) : name);
        };
        this.getLength = function()
        {
            return new Promise(function(resolve, refuse) {
                if (closed) {
                    refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                    return;
                }
    
                if (buffer) {
                    resolve(buffer.length);
                } else {
                    if (host.isRuntimeNodeFamily()) {
                        getLengthNode();
                        return;
                    }
                    else if (host.isRuntimeBrowserFamily()) {
                        getLengthXHR();
                        return;
                    }
                    else {
                        refuse(new Error(host.ERROR_RUNTIME_NOT_SUPPORTED, ""));
                        return;
                    }
                }
    
                function getLengthXHR() {
                    var xhr = new XMLHttpRequest();
                    xhr.open("HEAD", uri, true);
                    for (var h in own.headers) {
                        xhr.setRequestHeader(h, own.headers[h]);
                    }
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == xhr.DONE) {
                            acceptRanges = xhr.getResponseHeader("Accept-Ranges") == "bytes";
                            doGetLength(xhr.getResponseHeader("Content-Length"));
                        }
                    };
                    xhr.send();
                }
    
                function getLengthNode() {
                    var http = uri.scheme.toLowerCase() == mod.PROTOCOL_HTTP? require("http") : require("https");
    
                    try {
                        var req = http.request({
                            "method": "HEAD",
                            "protocol": uri.scheme.toLowerCase() + ":",
                            "hostname": uri.authority.host,
                            "port": uri.authority.port,
                            "path": uri.path + (uri.query? "?" + uri.query : ""), //+ (uri.fragment? "#" + uri.fragment : ""),
                            "headers": own.headers
                        }, function (res) {
                            res.on("error", error);
                            res.on("data", function () {
                                // ignore
                            });
                            res.on("end", function () {
                                if (res.statusCode >= 300 && res.statusCode < 400) {
                                    var headers = Object.keys(headers || res.headers);
                                    var lHeaders = headers.map(function (h) {return h.toLowerCase()});
                                    for (var i=0;i<lHeaders.length;i++) {
                                        if (lHeaders[i] === "location") {
                                            uri = ioURI.parse(res.headers[headers[i]]);
                                            getLengthNode();
                                            return;
                                        }
                                    }
                                }
    
                                acceptRanges = res.headers["accept-ranges"] == "bytes";
                                doGetLength(res.headers["content-length"]);
                            });
                        });
                        req.on("error", error);
                        req.end();
                    }
                    catch(e) {
                        error(e);
                    }
                }
    
                function doGetLength(len) {
                    if (!isNaN(len)) {
                        resolve(parseInt(len));
                    }
                    else {
                        refuse(new Error(mod.ERROR_UNSUPPORTED_OPERATION, ""));
                    }
                }
    
                function error(e) {
                    refuse(new Error(mod.ERROR_NETWORK, e));
                }
            });
        };
        this.read = function (len, position)
        {
            if (typeof position === "undefined") {
                position = 0;
            }
    
            return new Promise(function(resolve, refuse) {
                if (closed) {
                    refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                    return;
                }
    
                var total = 0;
                var initialPos = position;
                var initialLen = len;
                var lastProgress = null;
    
                function initHTTPRead() {
                    if (len != null) {
                        total = len;
                    }
    
                    if ((position > 0 || len != null) && acceptRanges == null) {
                        own.getLength().then(function (length) {
                            total = length;
                            initHTTPRead();
                        }, function(err) {
                            if (err.name != mod.ERROR_UNSUPPORTED_OPERATION) {
                                refuse(err);
                                return;
                            }
                            initHTTPRead();
                        });
                    }
                    else if ((position > 0 || len != null) && (acceptRanges || buffer)) {
                        if (len == null && buffer) {
                            total = buffer.length - position;
                        }
                        read();
                    }
                    else {
                        // it's not possible to do a range request, download and buffer, then read
                        download();
                    }
                }
    
                function download() {
                    if (host.isRuntimeNodeFamily()) {
                        downloadNode();
                        return;
                    }
                    else if (host.isRuntimeBrowserFamily()) {
                        downloadXHR();
                        return;
                    }
                    else {
                        refuse(new Error(host.ERROR_RUNTIME_NOT_SUPPORTED, ""));
                        return;
                    }
                }
    
                function downloadXHR() {
                    var xhr = new XMLHttpRequest();
                    xhr.open("GET", uri, true);
                    for (var h in own.headers) {
                        xhr.setRequestHeader(h, own.headers[h]);
                    }
                    xhr.onprogress = progressXHR;
                    xhr.responseType = "arraybuffer";
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState == xhr.DONE) {
                            if (xhr.status != 200) {
                                refuse(new Error(mod.ERROR_NETWORK, "The server returned code '" + xhr.status + "': '" + xhr.statusText + "'."));
                                return;
                            }
                            buffer = new Uint8Array(xhr.response);
                            read();
                        }
                    };
                    try {
                        xhr.send();
                    }
                    catch(e) {
                        error(e);
                    }
                }
    
                function downloadNode() {
                    var http = uri.scheme.toLowerCase() == mod.PROTOCOL_HTTP? require("http") : require("https");
                    var dataLen = 0;
                    var data = [];
    
                    try {
                        var req = http.request({
                            "method" : "GET",
                            "protocol": uri.scheme.toLowerCase() + ":",
                            "hostname": uri.authority.host,
                            "port": uri.authority.port,
                            "path": uri.path + (uri.query? "?" + uri.query : ""), // + (uri.fragment? "#" + uri.fragment : ""),
                            "headers": own.headers
                        }, function(res) {
                            res.on("response", function(resp) {
                                if (!isNaN(resp.headers["content-length"])) {
                                    total = parseInt(resp.headers["content-length"]);
                                }
                            }).on("error", error)
                                .on("data", function(chunk) {
                                    data.push(chunk);
                                    dataLen += chunk.length;
                                    progress(dataLen, total);
                                }).on("end", function() {
                                    if (res.statusCode >= 300 && res.statusCode < 400) {
                                        var headers = Object.keys(headers || res.headers);
                                        var lHeaders = headers.map(function (h) {return h.toLowerCase()});
                                        for (var i=0;i<lHeaders.length;i++) {
                                            if (lHeaders[i] === "location") {
                                                uri = ioURI.parse(res.headers[headers[i]]);
                                                downloadNode();
                                                return;
                                            }
                                        }
                                    }
                                    else if (res.statusCode != 200) {
                                        refuse(new Error(mod.ERROR_NETWORK, "The server returned code '" + res.statusCode + "'."));
                                        return;
                                    }
    
                                    buffer = new Buffer(dataLen);
                                    for (var i=0, l = data.length, pos = 0; i < l; i++) {
                                        data[i].copy(buffer, pos);
                                        pos += data[i].length;
                                    }
                                    read();
                                });
                        });
                        req.on("error", error);
                        req.end();
                    }
                    catch(e) {
                        error(e);
                    }
                }
    
                function read() {
                    if (buffer) {
                        if ((len == null && position == 0) || len == buffer.length) {
                            endHTTPRead(buffer);
                        }
                        else if (position >= buffer.length) {
                            refuse(new Error(ioStream.ERROR_INVALID_LENGTH, ""));
                        }
                        else {
                            var b = buffer.subarray(position, position + (len == null? buffer.length - position : len));
                            position += len;
                            endHTTPRead(b);
                        }
                    }
                    else {
                        if (host.isRuntimeNodeFamily()) {
                            readNode();
                            return;
                        }
                        else if (host.isRuntimeBrowserFamily()) {
                            readXHR();
                            return;
                        }
                        else {
                            refuse(new Error(host.ERROR_RUNTIME_NOT_SUPPORTED, ""));
                            return;
                        }
                    }
                }
    
                function readXHR() {
                    var xhr = new XMLHttpRequest();
                    xhr.open("GET", uri, true);
                    for (var h in own.headers) {
                        xhr.setRequestHeader(h, own.headers[h]);
                    }
                    xhr.onprogress = progressXHR;
                    xhr.responseType = "arraybuffer";
                    xhr.setRequestHeader("Range", "bytes=" + position + "-" + (len? position + len - 1 : ""));
                    xhr.onreadystatechange = function () {
                        // If the offset is valid, the server will return an HTTP 206 status code.
                        // If the offset is invalid, the request will return an HTTP 416 status code (Requested Range Not Satisfiable).
                        if (xhr.statusCode == 200) {
                            xhr.abort();
                            refuse(new Error(ioStream.ERROR_INVALID_LENGTH, xhr.statusText));
                            return;
                        }
                        if (xhr.readyState == xhr.DONE) {
                            if (xhr.status == 206) {
                                endHTTPRead(new Uint8Array(xhr.response));
                            }
                            else if (xhr.status == 416) {
                                refuse(new Error(ioStream.ERROR_INVALID_LENGTH, xhr.statusText));
                            }
                            else {
                                refuse(new Error(mod.ERROR_NETWORK, "The server returned code '" + xhr.status + "': '" + xhr.statusText + "'."));
                            }
                        }
                    };
                    try {
                        xhr.send();
                    }
                    catch(e) {
                        error(e);
                    }
                }
    
                function readNode() {
                    var http = uri.scheme.toLowerCase() == mod.PROTOCOL_HTTP? require("http") : require("https");
                    var dataLen = 0;
                    var data = [];
    
                    try {
                        var headers = {};
                        for (var h in own.headers) {
                            headers[h] = own.headers[h];
                        }
                        headers["range"] = "bytes=" + position + "-" + (len? position + len - 1 : "");
                        var req = http.request({
                            "method" : "GET",
                            "headers": headers,
                            "protocol": uri.scheme.toLowerCase() + ":",
                            "hostname": uri.authority.host,
                            "port": uri.authority.port,
                            "path": uri.path + (uri.query? uri.query : "") + (uri.fragment? uri.fragment : "")
                        }, function(res) {
                            if (!isNaN(res.headers["content-length"])) {
                                total = parseInt(res.headers["content-length"]);
                            }
                            if (res.statusCode == 200) {
                                refuse(new Error(ioStream.ERROR_INVALID_LENGTH, ""));
                                return;
                            }
    
                            res.on("error", error)
                                .on("data", function(chunk) {
                                    data.push(chunk);
                                    dataLen += chunk.length;
                                    progress(dataLen, total);
                                })
                                .on("end", function() {
                                    if (res.statusCode >= 300 && res.statusCode < 400) {
                                        var headers = Object.keys(headers || res.headers);
                                        var lHeaders = headers.map(function (h) {return h.toLowerCase()});
                                        for (var i=0;i<lHeaders.length;i++) {
                                            if (lHeaders[i] === "location") {
                                                uri = ioURI.parse(res.headers[headers[i]]);
                                                readNode();
                                                return;
                                            }
                                        }
                                    }
                                    else if (res.statusCode == 206) {
                                        var b = new Buffer(dataLen);
                                        for (var i=0, l = data.length, pos = 0; i < l; i++) {
                                            data[i].copy(b, pos);
                                            pos += data[i].length;
                                        }
                                        endHTTPRead(b);
                                    }
                                    else if (res.statusCode == 416) {
                                        refuse(new Error(ioStream.ERROR_INVALID_LENGTH, ""));
                                    }
                                    else {
                                        refuse(new Error(mod.ERROR_NETWORK, "The server returned code '" + res.statusCode + "'."));
                                    }
                                });
                        });
                        req.on("error", error);
                        req.end();
                    }
                    catch(e) {
                        error(e);
                    }
                }
    
                function progressXHR(evt) {
                    if (evt.lengthComputable) {
                        progress(evt.loaded, evt.total);
                    }
                }
    
                function progress(loaded, total) {
                    if (total) {
                        var currentProgress =  (loaded / total) * 100;
                        if (lastProgress != currentProgress) {
                            lastProgress = currentProgress;
                            own.events.fire(ioStream.EVENT_STREAM_READ_PROGRESS, new event.Progress({
                                "percentage" : currentProgress,
                                "operation" : {
                                    "type" : ioStream.OPERATION_STREAM_READ,
                                    "data" : {
                                        "position": initialPos,
                                        "length": initialLen,
                                        "total": total
                                    }
                                },
                                "promise" : this
                            }));
                        }
                    }
                }
    
                function error(e) {
                    refuse(new Error(mod.ERROR_NETWORK, e));
                }
    
                function endHTTPRead(b) {
                    progress(1,1);
                    resolve(b);
                }
    
                initHTTPRead();
            });
        };
        this.close = function ()
        {
            return new Promise(function(resolve, refuse) {
                if (closed) {
                    refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                    return;
                }
    
                buffer = null;
                closed = true;
    
                resolve();
            });
        };
    
        this.events = new event.Emitter(this);
    };
    StreamHTTP.open = function(uri, opt_access) {
        return new Promise(function(resolve, reject) {
            if (opt_access && opt_access != ioAccess.READ) {
                reject(new Error(mod.ERROR_UNSUPPORTED_OPERATION, "Writing to StreamHTTP is currently unsupported."));
            }
    
            resolve(new StreamHTTP(uri, opt_access));
        });
    };
    StreamHTTP.prototype = ioStream;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // URI Handler
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var mod = {};
    mod.parse = function(uri) {
        if(string.isURL(uri)) {
            if (uri.length >= 5 && uri.substr(0,5) == mod.PROTOCOL_HTTP + "://" ||
                uri.length >= 5 && uri.substr(0,5) == mod.PROTOCOL_HTTPS + "://") {
                return new ioURI(uri, self);
            }
        }
        else if (type.isString(uri)) {
            if (host.isRuntimeBrowserFamily() && !host.isRuntimeNodeFamily() && typeof location != "undefined" && location.protocol && location.host && location.pathname) {
                var idxLastSlash = location.pathname.lastIndexOf("/");
                var path = "/";
                if (idxLastSlash >= 0) {
                    path = location.pathname.substr(0, idxLastSlash + 1);
                }
    
                return new ioURI(location.protocol.substr(0, location.protocol.length - 1), location.host, uri.length > 0 && uri.substr(0, 1) == "/" ? uri : path + uri, null, null, self);
            }
        }
    };
    mod.open = function(uri, opt_access) {
        if (uri && type.isString(uri)) {
            uri = mod.parse(uri);
        }
        else if (uri && (typeof uri.scheme== "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        return StreamHTTP.open(uri, opt_access);
    };
    mod.exists = function(uri) {
        return new Promise(function(resolve, refuse) {
            if (uri && type.isString(uri)) {
                uri = mod.parse(uri);
            }
            else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
                uri = null;
            }
            var val = getItem(uri.path);
            if (val == null) {
                resolve();
                return;
            }
            resolve(ioURI.ENTRY_FILE);
        });
    
    };
    mod.delete = function(uri) {
        return new Promise(function(resolve, refuse) {
            if (uri && type.isString(uri)) {
                uri = mod.parse(uri);
            }
            else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
                uri = null;
            }
            if (!uri) {
                reject(new Error(ioURI.ERROR_INVALID_URI, ""));
                return;
            }
            var val = null;
            if (uri) {
                val = getItem(uri.path);
            }
            if (val == null) {
                refuse(new Error(ioURI.ERROR_NO_ENTRY, ""), "The file or directory does not exist local storage.");
                return;
            }
            removeItem(uri.path);
            resolve();
        });
    
    };
    mod.toString = function(uri, opt_format) {
        if (uri && type.isString(uri)) {
            uri = mod.parse(uri);
        }
        else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        if (!uri) {
            return "";
        }
        switch (opt_format) {
            default:
                return uri.toString();
        }
    };
    mod.StreamHTTP = StreamHTTP;
    mod.PROTOCOL_HTTP =  "http";
    mod.PROTOCOL_HTTPS = "https";
    mod.ERROR_NETWORK =  "Network Error";
    mod.ERROR_UNSUPPORTED_OPERATION =  "Unsupported Operation";
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Register File Protocol Handler
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    ioURI.protocols.register(mod, mod.PROTOCOL_HTTP,  [ ]);
    ioURI.protocols.register(mod, mod.PROTOCOL_HTTPS, [ ]);
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = mod;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://io-driver-http.1.0.0/io-driver-http.js

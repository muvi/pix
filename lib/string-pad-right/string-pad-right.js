/////////////////////////////////////////////////////////////////////////////////////
//
// module 'string-pad-right.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "string-pad-right.1.0.0/";
    define.parameters.pkx = {
        "name": "string-pad-right",
        "version": "1.0.0",
        "main": "string-pad-right.js",
        "repository": {
            "type": "git",
            "url": "https://gitlab.com/muvi/string-pad-right"
        },
        "dependencies": {
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "type": "https://gitlab.com/muvi/type#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "type": "https://gitlab.com/muvi/type#semver:^1.0"
            },
            "main": "string-pad-right.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // string-pad-right
    //
    //    Modifies a string to have a given number of a specified character at the end of a string.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var Error = require("error");
    var type =  require("type");
    
    function padRight(str, length, char) {
        return !type.isString(str) || length <= str.length? str : str + (new Array((length - str.length) + 1)).join(char || " ");
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    module.exports = padRight;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://string-pad-right.1.0.0/string-pad-right.js

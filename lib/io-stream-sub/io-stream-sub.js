/////////////////////////////////////////////////////////////////////////////////////
//
// module 'io-stream-buffered.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "io-stream-sub.1.0.0/";
    define.parameters.pkx = {
        "name": "io-stream-sub",
        "version": "1.0.0",
        "main": "io-stream-sub.js",
        "dependencies": {
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "event": "https://gitlab.com/muvi/event#semver:^1.0",
            "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "event": "https://gitlab.com/muvi/event#semver:^1.0",
                "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0"
            },
            "main": "io-stream-sub.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("event.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-stream.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // io-stream-sub
    //
    //    IO Sub Stream Class.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var Error =     require("error");
    var event =     require("event");
    var stream =    require("io-stream");

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // StreamSub Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function StreamSub(stream, offset, size, name) {
        var own = this;
        var closed = false;
        var events = new event.Emitter(this);

        this.getName = function () {
            return name;
        };
        this.getLength = function () {
            return new Promise(function (resolve, refuse) {
                closed ? refuse(new Error(stream.ERROR_STREAM_CLOSED, "")) : resolve(size);
            });
        };
        this.read = function (len, position) {
            if (!position) {
                position = 0;
            }

            return new Promise(function (resolve, refuse) {
                if (closed) {
                    refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                    return;
                }

                if (len == null) {
                    len = size;
                }

                if (offset + position + len > offset + size) {
                    len = (offset + size) - (offset + position);
                }
                return stream.read(len, offset + position).then(resolve).catch(refuse);
            });
        };
        this.write = function (data, position) {
            if (!position) {
                position = 0;
            }

            return new Promise(function (resolve, refuse) {
                if (closed) {
                    refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                    return;
                }

                return stream.write(data, offset + position).then(resolve).catch(refuse);
            });
        };
        this.close = function () {
            return new Promise(function (resolve, refuse) {
                if (closed) {
                    refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                    return;
                }

                closed = true;
                resolve();
            });
        };
    };
    StreamSub.prototype = stream;

    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = StreamSub;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://io-stream-buffered.1.0.0/io-stream-buffered.js

/////////////////////////////////////////////////////////////////////////////////////
//
// module 'string-to-uint8array.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "string-to-uint8array.1.0.0/";
    define.parameters.pkx = {
        "name": "string-to-uint8array",
        "version": "1.0.0",
        "main": "string-to-uint8array.js",
        "pkx": {
            "main": "string-to-uint8array.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // string-to-uint8array
    //
    //    Converts string to Uint8Array.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Polyfill
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    String.prototype.toUint8Array = function () {
        var binStr = this;
        var uA = new Uint8Array(binStr.length);
        Array.prototype.forEach.call(binStr, function (char, i) {
            uA[i] = char.charCodeAt(0);
        });
        return uA;
    };
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = String;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://string-to-uint8array.1.0.0/string-to-uint8array.js

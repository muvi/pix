/////////////////////////////////////////////////////////////////////////////////////
//
// module 'error.1.0.1/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "error.1.0.1/";
    define.parameters.pkx = {
        "name": "error",
        "version": "1.0.1",
        "main": "error.js",
        "pkx": {
            "main": "error.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // error
    //
    //    Library for standardizing error throwing.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var ERROR_UNKNOWN = "Unknown Error";
    var ERROR_UNSUPPORTED_OPERATION = "Unsupported Operation";
    var ERROR_INVALID_PARAMETER = "Invalid Parameter";
    
    var originalError = Error;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Error Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    Error = function(name, message, innerError, data) {
        this.innerError = null;
        this.data = null;
    
        if (!(this instanceof Error)) {
            throw new TypeError("Constructor 'Error' cannot be invoked without 'new'.");
        }
        else if (arguments.length >= 4) {
            this.data = data;
            this.innerError = innerError instanceof Error || innerError instanceof originalError ? innerError : null;
            this.message = message;
            this.name = name;
        }
        else if (arguments.length >= 3) {
            if (innerError instanceof Error || innerError instanceof originalError) {
                this.innerError = innerError;
            }
            else {
                this.data = innerError;
            }
            this.message = message;
            this.name = name;
        }
        else if (arguments.length == 2) {
            this.name = name;
            if (message instanceof Error || message instanceof originalError) {
                this.innerError = message;
            }
            else {
                this.message = message;
            }
        }
        else {
            this.name = ERROR_UNKNOWN;
            if (name instanceof Error) {
                this.innerError = name;
            }
            else {
                this.message = name;
            }
        }
    
        if (this.innerError) {
            // set stack to innerError stack
            this.stack = this.innerError.stack;
        }
        else if (originalError.captureStackTrace) {
            // stack trace in V8
            originalError.captureStackTrace(this, Error);
        }
        else {
            this.stack = (new originalError).stack;
        }
    };
    Error.prototype = Object.create(originalError.prototype);
    Error.prototype.name = "Error";
    if (originalError.captureStackTrace) {
        Object.defineProperties(Error, {
            "captureStackTrace" : {
                get : function() {
                    return originalError.captureStackTrace;
                },
                set : function (value) {
                    originalError.captureStackTrace = value;
                }
            }
        });
        Object.defineProperties(Error, {
            "prepareStackTrace" : {
                get : function() {
                    return originalError.prepareStackTrace;
                },
                set : function (value) {
                    originalError.prepareStackTrace = value;
                }
            }
        });
    }
    if (originalError.stackTraceLimit) {
        Object.defineProperties(Error, {
            "stackTraceLimit" : {
                get : function() {
                    return originalError.stackTraceLimit;
                },
                set : function (value) {
                    originalError.stackTraceLimit = value;
                }
            }
        });
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    module.exports = Error;
    module.exports.ERROR_UNKNOWN = ERROR_UNKNOWN;
    module.exports.ERROR_UNSUPPORTED_OPERATION = ERROR_UNSUPPORTED_OPERATION;
    module.exports.ERROR_INVALID_PARAMETER = ERROR_INVALID_PARAMETER;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://error.1.0.1/error.js

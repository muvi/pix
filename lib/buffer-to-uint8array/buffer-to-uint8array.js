/////////////////////////////////////////////////////////////////////////////////////
//
// module 'buffer-to-uint8array.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "buffer-to-uint8array.1.0.0/";
    define.parameters.pkx = {
        "name": "buffer-to-uint8array",
        "version": "1.0.0",
        "main": "buffer-to-uint8array.js",
        "pkx": {
            "main": "buffer-to-uint8array.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // buffer-to-uint8array
    //
    //    Converts node.js Buffer to Uint8Array.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Polyfill
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    if (typeof Buffer != "undefined" && typeof Buffer.prototype.toUint8Array == "undefined") {
        Buffer.prototype.toUint8Array = function() { 
            if (this.buffer)
            { 
                return new Uint8Array(this.buffer, this.byteOffset, this.byteLength);
            } 
            else { 
                var ab = new ArrayBuffer(this.length);
                var view = new Uint8Array(ab);
                for (var i = 0; i < this.length; ++i) { 
                    view[i] = this[i];
                } 
                return view;
            } 
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = typeof Buffer !== "undefined"? Buffer : {};
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://buffer-to-uint8array.1.0.0/buffer-to-uint8array.js

/////////////////////////////////////////////////////////////////////////////////////
//
// module 'type.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "type.1.0.0/";
    define.parameters.pkx = {
        "name": "type",
        "version": "1.0.0",
        "main": "type.js",
        "pkx": {
            "main": "type.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // type
    //
    //    Library for working with primitives.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var type;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Type Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function Type() {
        var self = this;
    
        this.TYPE_UNKNOWN = "unknown";
        this.TYPE_STRING = "string";
        this.TYPE_NUMBER = "number";
        this.TYPE_BOOLEAN = "boolean";
        this.TYPE_ARRAY = "array";
        this.TYPE_OBJECT = "object";
        this.TYPE_FUNCTION = "function";
    
        this.getType = function(obj) {
            if(self.isString(obj)) {
                return self.TYPE_STRING;
            }
            if(self.isBoolean(obj)) {
                return self.TYPE_BOOLEAN;
            }
            if(self.isNumber(obj)) {
                return self.TYPE_NUMBER;
            }
            if(self.isArray(obj)) {
                return self.TYPE_ARRAY;
            }
            if(self.isObject(obj)) {
                return self.TYPE_OBJECT;
            }
            if(self.isFunction(obj)) {
                return self.TYPE_FUNCTION;
            }
            return self.TYPE_UNKNOWN;
        };
        /**
         * Tests if an object is an array.
         * @param {object} obj - The object to test.
         * @returns Returns true if the given object is an array.
         **/
        this.isArray = function (obj) {
            return Object.prototype.toString.call(obj) === "[object Array]";
        };
        this.isString = function (obj) {
            return Object.prototype.toString.call(obj) === "[object String]";
        };
        this.isObject = function (obj) {
            return Object.prototype.toString.call(obj) === "[object Object]";
        };
        this.isNumber = function (obj) {
            return obj != null && obj !== true && obj !== false && !isNaN(obj);
        };
        this.isBoolean = function (obj) {
            return obj != null && obj.constructor === Boolean;
        };
        this.isFunction = function (obj) {
            var getType = {};
            return obj && getType.toString.call(obj) === "[object Function]";
        };
        this.isPrimitive = function (obj) {
            return self.isString(obj) || self.isNumber(obj) || self.isBoolean(obj);
        };
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Singleton Instance
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    type = new Type();
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = type;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://type.1.0.0/type.js

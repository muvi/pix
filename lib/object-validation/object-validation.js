/////////////////////////////////////////////////////////////////////////////////////
//
// module 'object-validation.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "object-validation.1.0.0/";
    define.parameters.pkx = {
        "name": "object-validation",
        "version": "1.0.0",
        "main": "object-validation.js",
        "dependencies": {
            "type": "https://gitlab.com/muvi/type#semver:^1.0",
            "validate": "https://gitlab.com/muvi/validate#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "type": "https://gitlab.com/muvi/type#semver:^1.0",
                "validate": "https://gitlab.com/muvi/validate#semver:^1.0"
            },
            "main": "object-validation.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.parameters.dependencies.push(define.cache.get("validate.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // object-validation
    //
    //    Library for processing and validating objects.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var validate = require("validate");
    var type =     require("type");
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Object class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function ObjectValidator() {
        var self = this;
    
        this.FUNCTION = "object-property-function";
        this.PROPERTY = "object-property-property";
    
        this.hasFunction = function(obj) {
            return scan(obj, "[object Function]");
        };
        this.hasProperty = function(obj) {
            for (var p in obj) {
                return true;
            }
            return false;
        };
    
        // validator
        this.getProperties = function(obj) {
            var props = [];
            if (self.hasFunction(obj)) { props.push(self.FUNCTION); }
            if (self.hasProperty(obj)) { props.push(self.PROPERTY); }
            return props;
        };
        this.isValid = function(obj) {
            return Object.prototype.toString.call(obj) === "[object Object]";
        };
    
        function scan(obj, type) {
            for (var p in obj) {
                var objPType = Object.prototype.toString.call(obj[p]);
                if (objPType == type ||
                    Object.getPrototypeOf(obj[p]) == type) {
                    return true;
                }
    
                // recursion
                //if (objPType == "[object Object]") {
                //    if (scan(obj[p], type)) {
                //        return true;
                //    }
                //}
            }
            return false;
        }
    }
    // set validator prototype
    ObjectValidator.prototype = validate.Validator;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = new ObjectValidator();
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://object-validation.1.0.0/object-validation.js

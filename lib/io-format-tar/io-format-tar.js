/////////////////////////////////////////////////////////////////////////////////////
//
// module 'io-format-tar.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "io-format-tar.1.0.0/";
    define.parameters.pkx = {
        "name": "io-format-tar",
        "version": "1.0.0",
        "title": "IO TAR Format Module",
        "description": "IO module that implements TAR stream support.",
        "license": "Apache-2.0",
        "main": "io-format-tar.js",
        "dependencies": {
            "type": "https://gitlab.com/muvi/type#semver:^1.0",
            "event": "https://gitlab.com/muvi/event#semver:^1.0",
            "io": "https://gitlab.com/muvi/io#semver:^1.0",
            "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
            "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0",
            "io-stream-sub": "https://gitlab.com/muvi/io-stream-sub#semver:^1.0",
            "io-volume": "https://gitlab.com/muvi/io-uri#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "type": "https://gitlab.com/muvi/type#semver:^1.0",
                "event": "https://gitlab.com/muvi/event#semver:^1.0",
                "io": "https://gitlab.com/muvi/io#semver:^1.0",
                "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
                "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0",
                "io-stream-sub": "https://gitlab.com/muvi/io-stream-sub#semver:^1.0",
                "io-volume": "https://gitlab.com/muvi/io-uri#semver:^1.0"
            },
            "main": "io-format-tar.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.parameters.dependencies.push(define.cache.get("event.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-uri.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-stream.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-stream-sub.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-volume.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // io-format-tar
    //
    //    IO module that implements TAR stream support.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var type =        require("type");
    var event =       require("event");
    var io =          require("io");
    var ioURI =       require("io-uri");
    var ioStream =    require("io-stream");
    var ioSubStream = require("io-stream-sub");
    var ioVolume =    require("io-volume");
    
    var TAR_TAG_TYPE_OBJECT = "tar-tag-type-object";
    
    var HEADER_USTAR = false;
    var HEADER_TYPE =  "ustar "; //watch the zero byte padding (last space after "ustar")
    var HEADER_SIZE =  512;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Functions
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function readString(header, pos, length) {
        var bVal = [];
        for (var i=pos;i<pos+length;i++)
        {
            bVal.push(header[i]);
        }
        return String.fromCharCode.apply(null, bVal).replace(/\0/g, ""); //remove 0 byte padding
    }
    
    function readLong(header, pos, length) {
        var l = 0;
        var str = readString(header, pos, length);
        if (str != "")
        {
            try
            {
                l = parseInt(str, 8);
            }
            catch(e)
            {
                //not an integer
            }
        }
        return l;
    }
    
    function readInt(header, pos, length) {
        var l = 0;
        var str = readString(header, pos, length);
        if (str != "")
        {
            try
            {
                l = parseInt(str, 8);
            }
            catch(e)
            {
                //not an integer
            }
        }
        return l;
    }
    
    function validateChecksum(header, checksum) {
        var sum = 0;
        for (var b = 0; b < header.length; b++)
        {
            if (b >= 148 && b < 148 + 8)
            {
                sum += 32;
            }
            else
            {
                sum += header[b];
            }
        }
        return sum == checksum;
    }
    
    function open(stream, options) {
        var reader = new TarReader(stream, options);
    
        var streamName;
        var streamPosition = 0;
        var objectCount = 0;
    
        function probe(header) {
            return new Promise(function(resolve, reject) {
                var tObj = new TarObject(header);
                if (!tObj.isValid)
                {
                    refuse(new Error(module.exports.ERROR_UNSUPPORTED_STREAM, "The stream does not seem to contain a valid tar file."));
                    return;
                }
                resolve();
            });
        }
        function getStreamName(reader) {
            return stream.getName(); 
        }
        function getStreamLength(name) {
            streamName = name;
    
            return stream.getLength(); 
        }
        function updateReader(length) {
            return new Promise(function(resolve, reject) {
                reader.file.name = streamName;
                reader.file.size = length;
                resolve();
            });
        }
        function readMetadata() {
            return new Promise(function(resolve, reject) {
                function readNextHeader() {
                    if (isNaN(streamPosition)) {
                        reject();
                        return;
                    }
                    return stream.read(HEADER_SIZE, streamPosition)
                        .then(processTarHeader)
                        .then(readNextHeader)
                        .catch(function handleTarError(e) { // when implementing multi file support, change "resolve" to "readNext"
                            if (e.name != ioStream.ERROR_INVALID_LENGTH) {
                                reject(e);
                                return;
                            }
                            resolve(reader);
                        });
                }
                return readNextHeader();
            });
        }
        function processTarHeader(header) {
            return new Promise(function(resolve, reject) {
                var tT = new TarTag("object" + objectCount, TAR_TAG_TYPE_OBJECT, header, options);
                var tObj = tT.value;
    
                objectCount++;
    
                tObj.offset = streamPosition + HEADER_SIZE;
                streamPosition += HEADER_SIZE;// + tObj.size
    
                if (!tObj.isValid && isNaN(tObj.size)) {
                    reject(module.exports.ERROR_CORRUPTED_TAR_FILE);
                    return;
                }
    
                // determine file length + zero bytes
                var wholeSize = Math.floor(tObj.size / parseFloat(HEADER_SIZE)) * HEADER_SIZE;
                if (wholeSize < tObj.size)
                {
                    wholeSize += HEADER_SIZE;
                }
                streamPosition += wholeSize;
    
                // if record is not empty, add to metadata
                if (tObj.name != "" && ((tObj.size > 0 && !tObj.isDirectory) || tObj.isDirectory))
                {
                    reader.metadata.tags.push(tT);
                }
    
                resolve();
            });
        }
    
        return new Promise(function(resolve, reject) {
            stream.read(HEADER_SIZE, 0)
                .then(probe)
                .then(getStreamName)
                .then(getStreamLength)
                .then(updateReader)
                .then(readMetadata)
                .then(resolve)
                .catch(reject);
        });
    }
    
    function mountVolume(uri, options) {
        var tarStream;
        var volume;

        if (typeof uri == "string") {
            uri = ioURI.parse(uri);
        }

        if (Object.getPrototypeOf(uri) == ioStream) {
            tarStream = uri;
            uri = null;
        }
        else if (!(uri instanceof ioURI)) {
            throw new TypeError("Mandatory parameter 'uri' should be of type 'URI' or 'Stream.");
        }
    
        function openTar(stream) {
            tarStream = stream;
    
            return open(tarStream, options);
        }
        function registerVolume(tarReader) {
            volume = new TarVolume(tarReader, options);
            return new Promise(function(resolve, reject) {
                io.volumes.register(volume);
                resolve(volume);
            });
        }
    
        return (uri? uri.open().then(openTar) : open(tarStream, options))
            .then(registerVolume);
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // TarObject Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function TarObject(header, options) {
        var self = this;
    
        this.name = null;
        this.size = null;
        this.offset = null;
        this.checksum = null;
        this.isValid = false;
        this.isDirectory = false;
    
        //read bytes into name property
        this.name = readString(header, 0, 100);
        var prefix = readString(header, 345, 155);
        if (prefix != "") {
            this.name = prefix + "/" + this.name;
        }
        this.size = readLong(header, 124, 12);
        this.checksum = readInt(header, 148, 8);
        this.isValid = header && header.length > 0? validateChecksum(header, self.checksum) : false;
    
        if (this.name.substr(this.name.length - 1) == "/") {
            this.name = this.name.substr(0, this.name.length - 1);
            this.isDirectory = true;
        }
    
        if (options && !isNaN(options.strip)) {
            for (var s=0;s<options.strip;s++) {
                var idx = self.name.indexOf("/");
                if (idx >= 0) {
                    self.name = self.name.substr(idx + 1);
                }
                else {
                    self.name = "";
                }
            }
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // TarReader Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function TarReader(stream, options) {
        this.err = [];
        this.metadata = new TarMetadata();
        this.file = {
            "name" : options? options.name : null,
            "size" : options? options.size : null
        }
    
        this.options = options || {};
    
        this.getTarObjectStream = function(obj, fileName)
        {
            return new ioSubStream(stream, obj.offset, obj.size, fileName);
        };
    
        this.close = function()
        {
            stream.close();
        };
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // TarTag Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function TarTag(key, type, data, options) {
        this.key = key;
        this.type = type;
        this.value = convertDataToValue(type, data);
        this.data = data;
    
        function convertDataToValue() {
            switch(type) {
                case TAR_TAG_TYPE_OBJECT:
                    return new TarObject(data, options);
                    break;
                default:
                    throw "Not Implemented";
                    break;
            }
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // TarMetadata Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function TarMetadata() {
        this.tags = [];
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // TarVolume Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function TarVolume(tarReader, options) {
        var self = this;
    
        // validate arguments
        if (!tarReader || !(tarReader instanceof TarReader)) {
            throw new Error(module.exports.ERROR_INVALID_TAR_VOLUME, "Mandatory parameter 'tarReader' should be of type 'TarReader'.");
        }
    
        this.err = [];
        this.id = "tar+" + (options? options.id : tarReader.file.name);
        this.name = (options? options.name : "TAR Volume") || (tarReader.file.name || "TAR Volume");
        this.protocol = null;
        this.description = "Tape Archive";
        this.state = ioVolume.STATE_READY;
        this.size = (options? options.size : null) || tarReader.file.size;
        this.type = ioVolume.TYPE_REMOVABLE;
        this.scope = ioVolume.SCOPE_LOCAL;
        this.class = ioVolume.CLASS_TEMPORARY;
        this.readOnly = true;
    
        this.open = function(path) {
            return new Promise(function(resolve, refuse) {
                for(var t=0;t<tarReader.metadata.tags.length;t++) {
                    var relPath = path.substr(0,1) == "/"? path.substr(1) : path;
                    var tarPath = tarReader.metadata.tags[t].value.name;
                    if (tarPath == relPath) {
                        resolve(tarReader.getTarObjectStream(tarReader.metadata.tags[t].value, relPath));
                        return;
                    }
                }
                refuse(new Error(ioURI.ERROR_NO_ENTRY, "File '" + path + "' does not exist in the tar volume '" + self.name + "'."));
            });
        };
    
        this.events = new event.Emitter(this);
    }
    TarVolume.prototype = ioVolume;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = { 
        "open" : open,
        "mountVolume" : mountVolume,
        "TarReader" : TarReader,
        "TarVolume" : TarVolume,
        "ERROR_UNSUPPORTED_STREAM" : "Unsupported Stream",
        "ERROR_INVALID_TAR_VOLUME" : "Invalid Tar Volume",
        "ERROR_CORRUPTED_TAR_FILE" : "Corrupted Tar File"
    };
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://io-format-tar.1.0.0/io-format-tar.js

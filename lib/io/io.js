/////////////////////////////////////////////////////////////////////////////////////
//
// module 'io.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "io.1.0.0/";
    define.parameters.pkx = {
        "name": "io",
        "version": "1.0.0",
        "main": "io.js",
        "dependencies": {
            "buffer-subarray": "https://gitlab.com/muvi/buffer-subarray#semver:^1.0",
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "event": "https://gitlab.com/muvi/event#semver:^1.0",
            "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
            "io-access": "https://gitlab.com/muvi/io-access#semver:^1.0",
            "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0",
            "io-volume": "https://gitlab.com/muvi/io-volume#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "buffer-subarray": "https://gitlab.com/muvi/buffer-subarray#semver:^1.0",
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "event": "https://gitlab.com/muvi/event#semver:^1.0",
                "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
                "io-access": "https://gitlab.com/muvi/io-access#semver:^1.0",
                "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0",
                "io-volume": "https://gitlab.com/muvi/io-volume#semver:^1.0"
            },
            "main": "io.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("buffer-subarray.1.0/"));
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("event.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-uri.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-access.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-stream.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-volume.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // io
    //
    //    Library for reading and writing data.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var event =  require("event");
    var access = require("io-access");
    var stream = require("io-stream");
    var uri =    require("io-uri");
    var volume = require("io-volume");
                 require("buffer-subarray");
    
    var io;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // IO Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function IO() {
        var self = this;
    
        var volumes = [];
        this.volumes = {};
        this.volumes.get = function (opt_id) {
            var found = [];
            for (var v in volumes) {
                if (!opt_id || volumes[v].id == opt_id) {
                    found.push(volumes[v]);
                }
            }
            return found;
        };
        this.volumes.register = function (volume) {
            var exists = false;
            for (var v in volumes) {
                if (volumes[v] == volume) {
                    exists = true;
                    break;
                }
            }
            if (exists) {
                throw new Error(self.ERROR_VOLUME_ALREADY_REGISTERED, "Volume '" + volume.name + "' is already registered.", volume);
            }
    
            volumes.push(volume);
    
            self.volumes.events.fire(self.EVENT_VOLUME_ADDED, volume);
        };
        this.volumes.unRegister = function (volume) {
            for (var v in volumes) {
                if (volumes[v] == volume) {
                    volumes.splice(v, 1);
                    self.volumes.events.fire(self.EVENT_VOLUME_REMOVED, volume);
                    return;
                }
            }
    
            throw new Error(self.ERROR_VOLUME_NOT_FOUND, "Volume '" + volume.name + "' was not registered.", volume);
        };
        this.volumes.events = new event.Emitter(this);
    }
    IO.prototype.ERROR_FILE_SIZE_EXEEDS_LIMIT =    "File Size Exeeds Limit";
    IO.prototype.EVENT_VOLUME_ADDED =              "volume-added";
    IO.prototype.EVENT_VOLUME_REMOVED =            "volume-removed";
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Singleton Instance
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    io = new IO();
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = io;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://io.1.0.0/io.js

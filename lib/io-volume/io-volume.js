/////////////////////////////////////////////////////////////////////////////////////
//
// module 'io-volume.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "io-volume.1.0.0/";
    define.parameters.pkx = {
        "name": "io-volume",
        "version": "1.0.0",
        "main": "io-volume.js",
        "dependencies": {
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "event": "https://gitlab.com/muvi/event#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "event": "https://gitlab.com/muvi/event#semver:^1.0"
            },
            "main": "io-volume.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("event.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // volume
    //
    //    Library for working with storage volumes.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    //
    // TODO - Add classes, types, ...
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var Error = require("error");
    var event = require("event");
    
    function unsupported() {
        return new Promise(function (resolve, refuse) {
            refuse(new Error(Error.ERROR_UNSUPPORTED_OPERATION, "The function called is not implemented."));
        });
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Volume Prototype
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var Volume = {
        id :          null,
    
        err :         [],
        name :        null,
        protocol :    null,
        size :        null,
        description : null,
        type :        null,
        class :       null,
        scope :       null,
        speed :       null,
        readOnly :    null,
    
        speed :       null,
    
        open :   unsupported,
        delete : unsupported,
        exists : unsupported,
        query :  unsupported,
        close :  unsupported,
        getURI : unsupported,
    
        // if implemented, needs to be overwritten
        events : new event.Emitter()
    };
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = Volume;
    module.exports.SCOPE_LOCAL =        "volume-scope-local";
    module.exports.SCOPE_REMOTE =       "volume-scope-remote";
    module.exports.CLASS_PERSISTENT =   "volume-class-persistent";
    module.exports.CLASS_TEMPORARY =    "volume-class-temporary";
    module.exports.SPEED_UNKOWN =       null;
    module.exports.SPEED_SLOW =         "volume-speed-slow";
    module.exports.SPEED_REASONABLE =   "volume-speed-reasonable";
    module.exports.SPEED_FAST =         "volume-speed-fast";
    module.exports.SPEED_INSTANT =      "volume-speed-instant";
    module.exports.STATE_LOCKED =       "volume-state-locked";
    module.exports.STATE_READY =        "volume-state-ready";
    module.exports.STATE_ERROR =        "volume-state-error";
    module.exports.TYPE_FIXED =         "volume-type-fixed";
    module.exports.TYPE_REMOVABLE =     "volume-type-removable";
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://io-volume.1.0.0/io-volume.js

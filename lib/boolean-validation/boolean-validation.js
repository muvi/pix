/////////////////////////////////////////////////////////////////////////////////////
//
// module 'boolean-validation.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "boolean-validation.1.0.0/";
    define.parameters.pkx = {
        "name": "boolean-validation",
        "version": "1.0.0",
        "main": "boolean-validation.js",
        "dependencies": {
            "validate": "https://gitlab.com/muvi/validate#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "validate": "https://gitlab.com/muvi/validate#semver:^1.0"
            },
            "main": "boolean-validation.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("validate.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // boolean-validation
    //
    //    Library for processing and validating booleans.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var validate = require("validate");
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Boolean class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function BooleanValidator() {
        var self = this;
    
        // validator
        this.getProperties = function(obj) {
            return [];
        };
        this.isValid = function(obj) {
            return Object.prototype.toString.call(obj) === "[object Boolean]";
        };
    }
    // set validator prototype
    BooleanValidator.prototype = validate.Validator;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = new BooleanValidator();
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://boolean-validation.1.0.0/boolean-validation.js

/////////////////////////////////////////////////////////////////////////////////////
//
// module 'string-to-buffer.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "string-to-buffer.1.0.0/";
    define.parameters.pkx = {
        "name": "string-to-buffer",
        "version": "1.0.0",
        "main": "string-to-buffer.js",
        "pkx": {
            "main": "string-to-buffer.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // string-to-buffer
    //
    //    Converts string to node.js Buffer.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Polyfill
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    if (typeof Buffer != "undefined" && typeof Buffer.prototype.toBuffer == "undefined") {
        String.prototype.toBuffer = function () {
            return Buffer.from(this);
        };
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = String;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://string-to-buffer.1.0.0/string-to-buffer.js

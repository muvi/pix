/////////////////////////////////////////////////////////////////////////////////////
//
// module 'uint8array-to-string.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "uint8array-to-string.1.0.0/";
    define.parameters.pkx = {
        "name": "uint8array-to-string",
        "version": "1.0.0",
        "main": "uint8array-to-string.js",
        "pkx": {
            "main": "uint8array-to-string.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // uint8array-to-string
    //
    //    Converts Uint8Array to string.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Polyfill
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    Uint8Array.prototype.toString = function () {
        var binStr = Array.prototype.map.call(this, function (char) {
            return String.fromCharCode(char);
        }).join("");
        return binStr;
    };
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = Uint8Array;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://uint8array-to-string.1.0.0/uint8array-to-string.js

/////////////////////////////////////////////////////////////////////////////////////
//
// module 'io-driver-localstorage.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "io-driver-localstorage.1.0.0/";
    define.parameters.pkx = {
        "name": "io-driver-localstorage",
        "version": "1.0.0",
        "main": "io-driver-localstorage.js",
        "dependencies": {
            "event": "https://gitlab.com/muvi/event#semver:^1.0",
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "type": "https://gitlab.com/muvi/type#semver:^1.0",
            "host": "https://gitlab.com/muvi/host#semver:^1.0",
            "io": "https://gitlab.com/muvi/io#semver:^1.0",
            "io-access": "https://gitlab.com/muvi/io-access#semver:^1.0",
            "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
            "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0",
            "io-volume": "https://gitlab.com/muvi/io-volume#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "event": "https://gitlab.com/muvi/event#semver:^1.0",
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "type": "https://gitlab.com/muvi/type#semver:^1.0",
                "host": "https://gitlab.com/muvi/host#semver:^1.0",
                "io": "https://gitlab.com/muvi/io#semver:^1.0",
                "io-access": "https://gitlab.com/muvi/io-access#semver:^1.0",
                "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
                "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0",
                "io-volume": "https://gitlab.com/muvi/io-volume#semver:^1.0"
            },
            "main": "io-driver-localstorage.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("event.1.0/"));
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.parameters.dependencies.push(define.cache.get("host.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-access.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-uri.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-stream.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-volume.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // io-driver-localstorage
    //
    //    IO Driver for accessing the runtimes localstorage.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Constants
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var PROTOCOL_LOCALSTORAGE = "ls";
    var MAX_SIZE =              "5242880"; //5MB max for local storage:
                                           //https://demo.agektmr.com/storage/
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var event =    require("event");
        Error =    require("error");
    var host =     require("host");
    var type =     require("type");
    var io =       require("io");
    var ioAccess = require("io-access");
    var ioURI =    require("io-uri");
    var ioStream = require("io-stream");
    var ioVolume = require("io-volume");

    var volume;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Functions
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function requireDependencies() {
        if (typeof localStorage === "undefined") {
            throw new Error(host.ERROR_RUNTIME_NOT_SUPPORTED, "The runtime does not support local storage.");
        }
    }
    function getItem(path) {
        return localStorage.getItem(PROTOCOL_LOCALSTORAGE + ":///" + (path.indexOf("/") == 0? path.substr(1) : path));
    }
    function setItem(path, value) {
        return localStorage.setItem(PROTOCOL_LOCALSTORAGE + ":///" + (path.indexOf("/") == 0? path.substr(1) : path), value);
    }
    function removeItem(path) {
        return localStorage.removeItem(PROTOCOL_LOCALSTORAGE + ":///" + (path.indexOf("/") == 0? path.substr(1) : path));
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // StreamFileSystem Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function StreamLocalStorage(buffer, key, access) {
        var own = this;
    
        var written = false;
        var closed = false;
    
        this.getName = function() {
            if (key && key.lastIndexOf("/") >= 0) {
                return key.substr(key.lastIndexOf("/"));
            }
            else {
                return key;
            }
        };
        this.getLength = function() {
            return new Promise(function(resolve, refuse) {
                requireDependencies();
    
                if (closed) {
                    refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                    return;
                }
                resolve(buffer.length);
            });
        };
        this.read = function (len, position) {
            if (!position) {
                position = 0;
            }
    
            return new Promise(function(resolve, refuse) {
                requireDependencies();
    
                if (closed) {
                    refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                    return;
                }
    
                if (len == null) {
                    return own.getLength().then(function (length) {
                        doRead(length - position);
                    }).catch(refuse);
                }
                else {
                    doRead(len);
                }
    
                function doRead(len) {
                    if (len == buffer.length) {
                        resolve(buffer);
                    }
                    else {
                        var nBuf = buffer.subarray(position, position + len);
                        position += len;
                        resolve(nBuf);
                    }
                }
            });
        };
        this.write = function(data, position) {
            if (!position) {
                position = 0;
            }
            
            return new Promise(function(resolve, refuse) {
                requireDependencies();
    
                if (closed) {
                    refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                    return;
                }
    
                if (data == null) {
                    resolve();
                    return;
                }
                if (!((typeof Buffer != "undefined" && data instanceof Buffer) || (typeof Uint8Array != "undefined" && data instanceof Uint8Array) || type.isString(data))) {
                    refuse(new Error(Error.ERROR_INVALID_PARAMETER, "Invalid parameter 'data'. The parameter should be of type 'Buffer', 'Uint8Array' or 'String'."));
                    return;
                }
                if (!type.isFunction(data.toString)) {
                    refuse(new Error(Error.ERROR_INVALID_PARAMETER, "Invalid parameter 'data'. The parameter should have the toString() function."));
                    return;
                }
                var newData;
                var newB;
                var newLength;
    
                newData = data instanceof Uint8Array? data : data.toUint8Array();
                newLength = position + newData.length;
                if (access == ioAccess.MODIFY && newLength < buffer.length) {
                    newLength = buffer.length;
                }
                newB = new Uint8Array(newLength);
                //only write previous contents if not access is modify and data is not text, or has been written once (overwrite)
                if (access == ioAccess.MODIFY || written) {
                    newB.set(buffer, 0);
                }
                else if (!written && type.isString(data)) {
                    newB.set(" ".repeat(position).toUint8Array(), 0);
                }
                newB.set(newData, position);
    
                buffer = newB;
                var bfrStr = buffer.toString();
                setItem(key, bfrStr);
                written = true;
                resolve();
            });
        };
        this.close = function (remove) {
            return new Promise(function(resolve, refuse) {
                requireDependencies();
    
                if (closed) {
                    refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                    return;
                }
    
                buffer = null;
                closed = true;
    
                if (remove) {
                    removeItem(key);
                }
    
                resolve();
            });
        };
    };
    StreamLocalStorage.open = function(uri, opt_access, opt_create) {
        return new Promise(function(resolve, reject) {
            requireDependencies();
    
            var buffer;
            var data = getItem(uri.toString());
    
            if (data) {
                buffer = new Uint8Array(data.length);
                Array.prototype.forEach.call(data, function (ch, i) {
                    buffer[i] = ch.charCodeAt(0);
                });
    
                resolve(new StreamLocalStorage(buffer, uri.toString(), opt_access));
                return;
            }
            else if (opt_access && opt_access != ioAccess.READ) {
                setItem(uri.toString(), "");
                resolve(new StreamLocalStorage(new Uint8Array(0), uri.toString(), opt_access));
                return;
            }
    
            reject(new Error(ioURI.ERROR_NO_ENTRY, ""));
        });
    };
    StreamLocalStorage.prototype = ioStream;

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // VolumeLocalStorage Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function VolumeLocalStorage(path) {
        this.err = [];
        this.id = "localstorage";
        this.name = "LocalStorage";
        this.protocol = PROTOCOL_LOCALSTORAGE;
        this.description = "Browser Storage";
        this.size = MAX_SIZE;
        this.state = ioVolume.STATE_READY;
        this.type = ioVolume.TYPE_FIXED;
        this.scope = ioVolume.SCOPE_LOCAL;
        this.class = ioVolume.CLASS_PERSISTENT;
        this.readOnly = false;
        var deviceId = "NOT IMPLEMENTED";
        this.localId = ""; //crypt.guid(crypt.md5(this.name + "/" + this.description + "/" + deviceId)); //TODO - deviceId from host library, but that would cause circular dependency

        var root = "ls://";
        
        this.getURI = function(path) {
            return new Promise(function(resolve, reject) { resolve(mod.parse(root + (path.indexOf("/") == 0? path : "/" + path))) });
        }
        this.query = function(uri) {  
            return new Promise(function(resolve, reject) { 
                if (uri && type.isString(uri)) {
                    uri = mod.parse(root + (uri.indexOf("/") == 0? "" : "/") + uri);
                }
                else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
                    uri = null;
                }

                if (!uri || uri.scheme != PROTOCOL_LOCALSTORAGE) {
                    reject("Invalid scheme."); //TODO - cleanup error
                    return;
                }
                var dir = uri.toString();
                var lastIdx = dir.lastIndexOf("/");
                if (lastIdx != dir.length - 1) {
                    dir = dir.substr(0, lastIdx + 1);
                }

                var items = [];
                for (var key in localStorage) {
                    if (key.indexOf(dir) == 0 && key.substr(dir.length).indexOf("/") == -1) {
                        items.push(mod.parse(key));
                    }
                }
                resolve(items);
            });
        };
        this.open = function(path, opt_access, opt_create) {
            return mod.open(root + path, opt_access, opt_create);
        };
        this.delete = function(path, opt_access, opt_create) {
            return mod.delete(root + path, opt_access, opt_create);
        };
        //TODO - TO IMPLEMENT 
        //this.getBytesUsed =      Function.prototype.bind.call(volume.getBytesUsed,      volume);
        //this.getBytesAvailable = Function.prototype.bind.call(volume.getBytesAvailable, volume);
        //this.close =             Function.prototype.bind.call(volume.close,             volume);

        this.events = new event.Emitter(this);
    };
    VolumeLocalStorage.prototype = ioVolume;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // URI Handler
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var mod = {};
    mod.parse = function(uri) {
        if(uri && type.isString(uri)) {
            if (uri.length >= 5 && uri.substr(0,5) == PROTOCOL_LOCALSTORAGE + "://") {
                return new ioURI(uri, mod);
            }
        }
    };
    mod.open = function(uri, opt_access, opt_create) {
        if (uri && type.isString(uri)) {
            uri = mod.parse(uri);
        }
        else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        if (!uri) {
            throw new Error(ioURI.ERROR_INVALID_URI, "");
        }
        return StreamLocalStorage.open(uri.path, opt_access, opt_create);
    };
    mod.exists = function(uri) {
        return new Promise(function(resolve, refuse) {
            requireDependencies();
    
            if (uri && type.isString(uri)) {
                uri = mod.parse(uri);
            }
            else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
                uri = null;
            }
            if (!uri) {
                refuse(new Error(ioURI.ERROR_INVALID_URI, ""));
                return;
            }
            if (uri.path == "/") {
                resolve(ioURI.ENTRY_DIRECTORY);
                return;
            }
            var val = getItem(uri.path || "");
            if (val == null) {
                refuse(new Error(ioURI.ERROR_NO_ENTRY, "File '" + uri.toString() + "' does not exist."));
                return;
            }
            resolve(ioURI.ENTRY_FILE);
        });
    
    };
    mod.delete = function(uri) {
        return new Promise(function(resolve, refuse) {
            requireDependencies();
    
            if (uri && type.isString(uri)) {
                uri = mod.parse(uri);
            }
            else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
                uri = null;
            }
            if (!uri) {
                reject(new Error(ioURI.ERROR_INVALID_URI, ""));
                return;
            }
            var val = null;
            if (uri) {
                val = getItem(uri.path);
            }
            if (val == null) {
                refuse(new Error(ioURI.ERROR_NO_ENTRY, ""), "The file or directory does not exist local storage.");
                return;
            }
            removeItem(uri.path);
            resolve();
        });
    };
    mod.mount = function(uri, opt_access) {
        if (uri && type.isString(uri)) {
            uri = mod.parse(uri);
        }
        else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        if (!uri) {
            throw new Error(ioURI.ERROR_INVALID_URI, "");
        }
        return new VolumeLocalStorageSystem(uri.path);
    };
    mod.toString = function(uri, opt_format) {
        if (uri && type.isString(uri)) {
            uri = mod.parse(uri);
        }
        else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        if (!uri) {
            return "";
        }
        switch (opt_format) {
            default:
                return uri.toString();
        }
    };
    mod.getVolume = function() {
        return new Promise(function(resolve, refuse) {
            requireDependencies();

            if (volume) {
                resolve(volume);
            }
            else {
                reject(); //TODO - return error -> no volumes?
            }
        });
    };
    mod.StreamLocalStorage = StreamLocalStorage;
    mod.VolumeLocalStorage = VolumeLocalStorage;
    mod.PROTOCOL_LOCALSTORAGE = PROTOCOL_LOCALSTORAGE;

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Create LocalStorage Root Volume
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    volume = new VolumeLocalStorage("/");
    io.volumes.register(volume);
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Register LS Protocol Handler
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    ioURI.protocols.register(mod, PROTOCOL_LOCALSTORAGE);
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = mod;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://io-driver-localstorage.1.0.0/io-driver-localstorage.js

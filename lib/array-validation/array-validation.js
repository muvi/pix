/////////////////////////////////////////////////////////////////////////////////////
//
// module 'array-validation.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "array-validation.1.0.0/";
    define.parameters.pkx = {
        "name": "array-validation",
        "version": "1.0.0",
        "main": "array-validation.js",
        "dependencies": {
            "type": "https://gitlab.com/muvi/type#semver:^1.0",
            "validate": "https://gitlab.com/muvi/validate#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "type": "https://gitlab.com/muvi/type#semver:^1.0",
                "validate": "https://gitlab.com/muvi/validate#semver:^1.0"
            },
            "main": "array-validation.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.parameters.dependencies.push(define.cache.get("validate.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // array-validation
    //
    //    Library for processing and validating arrays.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var validate = require("validate");
    var type =     require("type");
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Array class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function ArrayValidator() {
        var self = this;
    
        this.STRING =   "array-property-string";
        this.NUMBER =   "array-property-number";
        this.BOOLEAN =  "array-property-boolean";
        this.ARRAY =    "array-property-array";
        this.OBJECT =   "array-property-object";
        this.FUNCTION = "array-property-function";
        this.UNKNOWN =  "array-property-unknown";
    
        this.isValid = function(obj) {
            return Object.prototype.toString.call(obj) === "[object Array]";
        };
        this.hasString = function(obj) {
            return scan(obj, type.TYPE_STRING);
        };
        this.hasNumber = function(obj) {
            return scan(obj, type.TYPE_NUMBER);
        };
        this.hasBoolean = function(obj) {
            return scan(obj, type.TYPE_BOOLEAN);
        };
        this.hasArray = function(obj) {
            return scan(obj, type.TYPE_ARRAY);
        };
        this.hasObject = function(obj) {
            return scan(obj, type.TYPE_OBJECT);
        };
        this.hasFunction = function(obj) {
            return scan(obj, type.TYPE_FUNCTION);
        };
        this.hasUnknown = function(obj) {
            return scan(obj, type.TYPE_UNKNOWN);
        };
    
        // validator
        this.getProperties = function(obj) {
            var props = [];
            if (self.hasString(obj)) { props.push(self.STRING); }
            if (self.hasNumber(obj)) { props.push(self.NUMBER); }
            if (self.hasBoolean(obj)) { props.push(self.BOOLEAN); }
            if (self.hasArray(obj)) { props.push(self.ARRAY); }
            if (self.hasObject(obj)) { props.push(self.OBJECT); }
            if (self.hasFunction(obj)) { props.push(self.FUNCTION); }
            if (self.hasUnknown(obj)) { props.push(self.UNKNOWN); }
            return props;
        };
    
        function scan(obj, t) {
            for (var p in obj) {
                if (isNaN(p)) {
                    continue;
                }
                var objPType = type.getType(obj[p]);
                if (objPType == t) {
                    return true;
                }
            }
            return false;
        }
    }
    // set validator prototype
    ArrayValidator.prototype = validate.Validator;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = new ArrayValidator();
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://array-validation.1.0.0/array-validation.js

/////////////////////////////////////////////////////////////////////////////////////
//
// module 'io-stream-buffered.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "io-stream-buffered.1.0.0/";
    define.parameters.pkx = {
        "name": "io-stream-buffered",
        "version": "1.0.0",
        "main": "io-stream-buffered.js",
        "dependencies": {
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "event": "https://gitlab.com/muvi/event#semver:^1.0",
            "type": "https://gitlab.com/muvi/type#semver:^1.0",
            "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "event": "https://gitlab.com/muvi/event#semver:^1.0",
                "type": "https://gitlab.com/muvi/type#semver:^1.0",
                "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0"
            },
            "main": "io-stream-buffered.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("event.1.0/"));
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-stream.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // io-stream-buffered
    //
    //    IO Buffered Stream Class.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var Error =     require("error");
    var event =     require("event");
    var type  =     require("type");
    var stream =    require("io-stream");
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // StreamBuffered Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function StreamBuffered(buffer, name) {
        var own = this;
        var closed = false;
        var events = new event.Emitter(this);
    
        this.getName = function () {
            return name;
        };
        this.getLength = function () {
            return new Promise(function (resolve, refuse) {
                closed ? refuse(new Error(stream.ERROR_STREAM_CLOSED, "")) : resolve(buffer.length);
            });
        };
        this.read = function (len, position) {
            if (!position) {
                position = 0;
            }
    
            return new Promise(function (resolve, refuse) {
                if (closed) {
                    refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                    return;
                }
                if (len == null) {
                    return own.getLength().then(function (length) {
                        doRead(length - position);
                    }).catch(refuse);
                }
                else {
                    doRead(len);
                }
    
                function doRead(len) {
                    if (len == buffer.length) {
                        resolve(buffer);
                    }
                    else {
                        var nBuf = buffer.subarray(position, position + len);
                        position += len;
                        resolve(nBuf);
                    }
                }
            });
        };
        this.write = function (data, position) {
            if (!position) {
                position = 0;
            }
    
            return new Promise(function (resolve, refuse) {
                if (closed) {
                    refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                    return;
                }
    
                if (data == null) {
                    resolve();
                    return;
                }
                if (!((typeof Buffer != "undefined" && data instanceof Buffer) || (typeof Uint8Array != "undefined" && data instanceof Uint8Array) || type.isString(data))) {
                    refuse(new Error(Error.ERROR_INVALID_PARAMETER, "Mandatory parameter 'data' should be of type 'Buffer', 'Uint8Array' or 'String'."));
                    return;
                }
                if (!type.isFunction(data.toString)) {
                    refuse(new Error(Error.ERROR_INVALID_PARAMETER, "Mandatory parameter 'data' should have the toString() function."));
                    return;
                }
                var newData;
                var newB;
                var newLength;
    
                newData = data instanceof Uint8Array ? data : data.toUint8Array();
                newLength = position + newData.length;
                newB = new Uint8Array(newLength);
                newB.set(buffer, 0);
                newB.set(newData, position);
    
                buffer = newB;
                resolve();
            });
        };
        this.close = function () {
            return new Promise(function (resolve, refuse) {
                if (closed) {
                    refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                    return;
                }
    
                buffer = null;
    
                closed = true;
                resolve();
            });
        };
    };
    StreamBuffered.prototype = stream;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = StreamBuffered;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://io-stream-buffered.1.0.0/io-stream-buffered.js

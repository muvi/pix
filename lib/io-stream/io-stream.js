/////////////////////////////////////////////////////////////////////////////////////
//
// module 'io-stream.1.0.1/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "io-stream.1.0.1/";
    define.parameters.pkx = {
        "name": "io-stream",
        "version": "1.0.1",
        "main": "io-stream.js",
        "dependencies": {
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "type": "https://gitlab.com/muvi/type#semver:^1.0",
            "mime-type": "https://gitlab.com/muvi/mime-type#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "type": "https://gitlab.com/muvi/type#semver:^1.0",
                "mime-type": "https://gitlab.com/muvi/mime-type#semver:^1.0"
            },
            "main": "io-stream.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.parameters.dependencies.push(define.cache.get("mime-type.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // io-stream
    //
    //    IO Stream Prototype library.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Constants
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var ERROR_STREAM_CLOSED =     "Stream Closed";
    var ERROR_INVALID_JSON_DATA = "Invalid JSON Data";
    var ERROR_INVALID_LENGTH =    "Invalid Length";
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var Error =     require("error");
    var type  =     require("type");
    var mimeType  = require("mime-type");
    
    function unsupported() {
        return new Promise(function (resolve, refuse) {
            refuse(new Error(Error.ERROR_UNSUPPORTED_OPERATION, "The function called is not implemented."));
        });
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Stream Prototype
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var Stream = {
        objectURL : null,
    
        getAccess : unsupported,
        getName : unsupported,
        getLength : unsupported,
        read : unsupported,
        readAsString : function () {
            var own = this;
            return new Promise(function (resolve, refuse) {
                return own.read(null, 0).then(function (bytes) {
                    resolve(bytes.toString());
                }).catch(refuse);
            });
        },
        readAsJSON : function () {
            var own = this;
            return new Promise(function (resolve, refuse) {
                return own.readAsString().then(function (str) {
                    try {
                        resolve(JSON.parse(str));
                    }
                    catch (e) {
                        refuse(new Error(ERROR_INVALID_JSON_DATA, e));
                    }
                }).catch(refuse);
            });
        },
        copyTo : function (stream) {
            var own = this;
            return new Promise(function (resolve, refuse) {
                return own.read(null, 0).then(function (bytes) {
                    stream.write(bytes, 0).then(resolve).catch(refuse);
                }).catch(refuse);
            });
        },
        write : unsupported,
        getMimeType : function () {
            var own = this;
            return new Promise(function (resolve, refuse) {
                return own.read(16, 0).then(function (arr) {
                    resolve(mimeType(arr, own.getName()));
                }).catch(refuse);
            });
        },
        createObjectURL : function () {
            var own = this;
    
            return new Promise(function (resolve, refuse) {
                if (own.objectURL) {
                    resolve(own.objectURL);
                    return;
                }
    
                if (typeof URL == "undefined" && typeof btoa == "undefined") {
                    refuse(new Error(Error.ERROR_UNSUPPORTED_OPERATION, "The environment does not support creating an object url."));
                    return;
                }
    
                return own.read(null, 0).then(function (arr) {
                    return own.getMimeType().then(function (mimeType) {
                        if (typeof URL !== "undefined" && type.isFunction(URL.createObjectURL)) {
                            var blob = new Blob([arr], {"type": mimeType});
                            own.objectURL = URL.createObjectURL(blob);
                        }
                        else {
                            own.objectURL = "data:" + mimeType + ";base64," + btoa(bytes.toString());
                        }
                        resolve(own.objectURL);
                    }).catch(refuse);
                }).catch(refuse);
            });
        },
        close : unsupported
    };
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = Stream;
    module.exports.ERROR_INVALID_LENGTH = ERROR_INVALID_LENGTH;
    module.exports.ERROR_STREAM_CLOSED = ERROR_STREAM_CLOSED;
    module.exports.ERROR_INVALID_JSON_DATA = ERROR_INVALID_JSON_DATA;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://io-stream.1.0.1/io-stream.js

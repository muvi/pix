/////////////////////////////////////////////////////////////////////////////////////
//
// module 'object-merge.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "object-merge.1.0.0/";
    define.parameters.pkx = {
        "name": "object-merge",
        "version": "1.0.0",
        "main": "object-merge.js",
        "dependencies": {
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "type": "https://gitlab.com/muvi/type#semver:^1.0",
            "object-clone": "https://gitlab.com/muvi/object-clone#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "type": "https://gitlab.com/muvi/type#semver:^1.0",
                "object-clone": "https://gitlab.com/muvi/object-clone#semver:^1.0"
            },
            "main": "object-merge.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.parameters.dependencies.push(define.cache.get("object-clone.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // object-merge
    //
    //    Library for merging javascript objects.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var Error = require("error");
    var type =  require("type");
    var clone = require("object-clone");
    
    var ERROR_INVALID_OBJECT = "Invalid Object";
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Functions
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function merge(source1, source2, overwriteArray) {
        /*
        * Properties from the Source1 object will be copied to Source2 Object. Source1 will overwrite any existing attributes
        * Note: This method will return a new merged object, Source1 and Source2 original values will not be replaced.
        * */
        if(source1 == null || !type.isObject(source1)){
            throw new Error(ERROR_INVALID_OBJECT, "Parameter 'source1' is of type '" + (typeof source1) + "', while null or an object is expected.");
        }
        if(source2 == null || !type.isObject(source2)){
            throw new Error(ERROR_INVALID_OBJECT, "Parameter 'source2' is of type '" + (typeof source2) + "', while null or an object is expected.");
        }
    
        var mergedJSON = clone(source2);
    
        for (var attrname in source1) {
            if(mergedJSON.hasOwnProperty(attrname)) {
                if ( source1[attrname]!=null) {
                    if (source1[attrname].constructor==Object) {
                        /*
                        * Recursive call if the property is an object,
                        * Iterate the object and set all properties of the inner object.
                        */
                        mergedJSON[attrname] = merge(source1[attrname], mergedJSON[attrname], overwriteArray);
                    }
                    else if (type.isArray(source1[attrname]) && !overwriteArray) {
                        mergedJSON[attrname] = mergedJSON[attrname].concat(source1[attrname]);
                    }
                    else {
                        mergedJSON[attrname] = source1[attrname];
                    }
                }
            } else {
                //else copy the property from source1
                mergedJSON[attrname] = source1[attrname];
            }
        }
    
        return mergedJSON;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = merge;
    module.exports.ERROR_INVALID_OBJECT = ERROR_INVALID_OBJECT;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://object-merge.1.0.0/object-merge.js

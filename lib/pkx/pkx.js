/////////////////////////////////////////////////////////////////////////////////////
//
// module 'pkx.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "pkx.1.0.0/";
    define.parameters.pkx = {
        "name": "pkx",
        "version": "1.0.0",
        "main": "pkx.js",
        "dependencies": {
            "host": "https://gitlab.com/muvi/host#semver:^1.0",
            "event": "https://gitlab.com/muvi/event#semver:^1.0",
            "io": "https://gitlab.com/muvi/io#semver:^1.0",
            "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
            "io-volume": "https://gitlab.com/muvi/io-volume#semver:^1.0",
            "io-format-tar": "https://gitlab.com/muvi/io-format-tar#semver:^1.0",
            "io-format-gzip": "https://gitlab.com/muvi/io-format-gzip#semver:^1.0",
            "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0",
            "io-stream-buffered": "https://gitlab.com/muvi/io-stream-buffered#semver:^1.0",
            "validate": "https://gitlab.com/muvi/validate#semver:^1.0",
            "string-validation": "https://gitlab.com/muvi/string-validation#semver:^1.0",
            "boolean-validation": "https://gitlab.com/muvi/boolean-validation#semver:^1.0",
            "array-validation": "https://gitlab.com/muvi/array-validation#semver:^1.0",
            "object-validation": "https://gitlab.com/muvi/object-validation#semver:^1.0",
            "object-merge": "https://gitlab.com/muvi/object-merge#semver:^1.0",
            "type": "https://gitlab.com/muvi/type#semver:^1.0",
            "version": "https://gitlab.com/muvi/version#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "host": "https://gitlab.com/muvi/host#semver:^1.0",
                "event": "https://gitlab.com/muvi/event#semver:^1.0",
                "io": "https://gitlab.com/muvi/io#semver:^1.0",
                "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
                "io-volume": "https://gitlab.com/muvi/io-volume#semver:^1.0",
                "io-format-tar": "https://gitlab.com/muvi/io-format-tar#semver:^1.0",
                "io-format-gzip": "https://gitlab.com/muvi/io-format-gzip#semver:^1.0",
                "io-stream": "https://gitlab.com/muvi/io-stream#semver:^1.0",
                "io-stream-buffered": "https://gitlab.com/muvi/io-stream-buffered#semver:^1.0",
                "validate": "https://gitlab.com/muvi/validate#semver:^1.0",
                "string-validation": "https://gitlab.com/muvi/string-validation#semver:^1.0",
                "boolean-validation": "https://gitlab.com/muvi/boolean-validation#semver:^1.0",
                "array-validation": "https://gitlab.com/muvi/array-validation#semver:^1.0",
                "object-validation": "https://gitlab.com/muvi/object-validation#semver:^1.0",
                "object-merge": "https://gitlab.com/muvi/object-merge#semver:^1.0",
                "type": "https://gitlab.com/muvi/type#semver:^1.0",
                "version": "https://gitlab.com/muvi/version#semver:^1.0"
            },
            "main": "pkx.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("host.1.0/"));
    define.parameters.dependencies.push(define.cache.get("event.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-uri.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-volume.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-format-tar.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-format-gzip.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-stream.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-stream-buffered.1.0/"));
    define.parameters.dependencies.push(define.cache.get("validate.1.0/"));
    define.parameters.dependencies.push(define.cache.get("string-validation.1.0/"));
    define.parameters.dependencies.push(define.cache.get("boolean-validation.1.0/"));
    define.parameters.dependencies.push(define.cache.get("array-validation.1.0/"));
    define.parameters.dependencies.push(define.cache.get("object-validation.1.0/"));
    define.parameters.dependencies.push(define.cache.get("object-merge.1.0/"));
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.parameters.dependencies.push(define.cache.get("version.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // pkx
    //
    //    Using extension module that enables the loading of pkx packages.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var host =             require("host");
    var event =            require("event");
    var io =               require("io");
    var ioURI =            require("io-uri");
    var ioVolume =         require("io-volume");
    var tar =              require("io-format-tar");
    var gzip =             require("io-format-gzip");
    var ioStream =         require("io-stream");
    var ioBufferedStream = require("io-stream-buffered");
    var validate =         require("validate");
    var string =           require("string-validation");
    var object =           require("object-validation");
    var boolean =          require("boolean-validation");
    var array =            require("array-validation");
    var type =             require("type");
    var version =          require("version");
    var merge =            require("object-merge");
    
    var PKX_SYSTEM =              "pkx";
    var PKX_FILE_EXTENSION =      "." + PKX_SYSTEM;
    var PKX_DESCRIPTOR_FILENAME = "package.json";
    var PROTOCOL_PKX =            PKX_SYSTEM;
    var DEPENDENCY_PKX =          PKX_SYSTEM;
    var DEPENDENCY_CONFIG =       "configuration";
    var DEPENDENCY_REQUIRER =     "requirer";
    var INDENT_OFFSET =           4;
    
    var ERROR_UNSUPPORTED_OPERATION =     "Unsupported Operation";
    var ERROR_TARGET_MISMATCH =           "Target Mismatch";
    var ERROR_DEPENDENCY =                "Dependency Error";
    var ERROR_NO_ENTRY_POINT =            "No Entry Point";
    var ERROR_INVALID_PKX_SELECTOR =      "Invalid Selector";
    var ERROR_INVALID_PKX_DESCRIPTOR =    "Invalid Package Descriptor";
    var ERROR_INVALID_REQUEST_PROCESSOR = "Invalid Request Processor";
    var ERROR_INVALID_PKX_VOLUME =        "Invalid PKX Volume";
    
    var init = false;
    var processors =          {};
    var processing =          {};
    var volumes =             [];
    var requested =           {};
    var streamParseFileDone = {};
    var streamObjectURIs =    {};
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Functions
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function findVolume(selector) {
        //var id = selector.package + "/" + (selector.resource? selector.resource : "");
        for (var v in volumes) {
            if (version.compare(selector.id, v, selector.upgradable) || selector.id == v) {
                return volumes[v];
            }
        }
    }
    
    function mountVolume(uri, options) {
        return new Promise(function(resolve, reject) {
            if (typeof uri == "string") {
                uri = ioURI.parse(uri);
            }
        
            // validate arguments
            if (!(uri instanceof ioURI)) {
                throw new TypeError("Mandatory parameter 'uri' should be of type 'URI'.");
            }
        
            var gzipStream;
            var tarStream;
            var tarVolume;
            var pkxJSONStream;
            var pkxVolume;

            var volumePromise;
            if (uri.isDirectory) {
                volumePromise = uri.mount();
            } else {
                volumePromise = uri.open() 
                    .then(openGzip)
                    .then(getTarFromGzip)
                    .then(openTar)
                    .catch(reject)
            }

            volumePromise
                .then(openPackageJSON)
                .then(readPackageJSON)
                .then(parsePackageJSON)
                .then(closeStreamsAndResolve)
                .catch(reject)
                ;
        
            function openGzip(stream) {
                gzipStream = stream;
        
                if (options && options.headers && gzipStream.headers) {
                    gzipStream.headers = options.headers;
                }
        
                return gzip.open(gzipStream);
            }
            function getTarFromGzip(gzipReader) {
                return new Promise(function(resolve, reject) {
                    for(var t=0;t<gzipReader.metadata.tags.length;t++) {
                        if (gzipReader.metadata.tags[t].type == gzip.GZIP_TAG_TYPE_OBJECT) {
                            stream = gzipReader.getGZipObjectStream(gzipReader.metadata.tags[t].value);
                            return resolve(stream);
                        }
                    }
                    throw new Error(ERROR_INVALID_PKX_VOLUME, "The gzip stream does not seem to contain a file.");
                });
            }
            function openTar(stream) {    
                tarStream = stream;
                return tar.mountVolume(stream, options.strip? { "strip" : options.strip, "id" : uri.toString() } : null);
            }
            function openPackageJSON(volume) {
                tarVolume = volume;
                return volume.open("/" + PKX_DESCRIPTOR_FILENAME);
            }
            function readPackageJSON(stream) {
                pkxJSONStream = stream;
                return stream.readAsJSON();
            }
            function parsePackageJSON(pkxJSON) {
                return new Promise(function(resolve, reject) {
                    // validate pkx
                    var pkx = new PKX(pkxJSON);
                        
                    pkxVolume = new PKXVolume(tarVolume, pkx);
    
                    io.volumes.register(pkxVolume);
    
                    if (!volumes[pkxVolume.pkx.id]) {
                        volumes[pkxVolume.pkx.id] = pkxVolume;
                    }
                    else {
                        volumes[pkxVolume.pkx.id + " <" + volumes.length + ">"] = pkxVolume;
                        if (console) {
                            console.warn("A PKXVolume with id '" + pkxVolume.pkx.id + "' was already mounted and still present in cache.");
                        }
                    }
    
                    resolve();
                });
            }
            function closeStreamsAndResolve() {
                if (pkxJSONStream) {
                    pkxJSONStream.close().catch(console.warn);
                }
                if (gzipStream) {
                    gzipStream.close().catch(console.warn);
                }

                resolve(pkxVolume);
            }
        });
    }

    function replaceURIsInHTML(pkxPath, html) {
        return new Promise(function(resolve, reject) {
            var count = 0;
            var error;

            var path = pkxPath;

            // first pass, get all pkx uris
            var uris = replaceAllRelativeURIs(false, html, true, path);

            if (Object.keys(uris).length == 0) {
                resolve(html);
                return;
            }

            // create object url
            for (var u in uris) {
                getObjectURL(u, uris).then(function() {
                    count++;
                }).catch(function(e) {
                    count++;
                    error = error || [];
                    error.push(e);
                }).finally(function() {
                    if (count == Object.keys(uris).length) {
                        if(error) {
                            reject(error);
                        }
                        else {
                            resolve(replaceAllRelativeURIs(false, html, false, uris, path));
                        }
                    }
                });
            }
        });
    }
    function replaceURIsInCSS(pkxPath, css) {
        return new Promise(function(resolve, reject) {
            var count = 0;
            var error;

            var path = pkxPath;

            // first pass, get all pkx uris
            var uris = replaceAllRelativeURIs(true, css, true, path);

            if (Object.keys(uris).length == 0) {
                resolve(css);
                return;
            }

            // create object url
            for (var u in uris) {
                getObjectURL(u, uris).then(function() {
                    count++;
                }).catch(function(e) {
                    count++;
                    error = error || [];
                    error.push(e);
                }).finally(function() {
                    if (count == Object.keys(uris).length) {
                        if(error) {
                            reject(error);
                        }
                        else {
                            resolve(replaceAllRelativeURIs(true, css, false, uris, path));
                        }
                    }
                });
            }
        });
    }
    function getObjectURL(uri, uriList) {
        return new Promise(function(resolve, reject) {
            // mount pkx volume
            return ioURI.open(uri).then(function(stream) {
                return stream.createObjectURL().then(function(objectUrl) {
                    uriList[uri] = objectUrl;
                    resolve();
                }).catch(reject);
            }).catch(reject);
        });
    }
    function relativeURItoAbsolute(url, path) {
        /* Only accept commonly trusted protocols:
        * Only data-image URLs are accepted, Exotic flavours (escaped slash,
        * html-entitied characters) are not supported to keep the function fast */
        if(/^(https?|file|ftps?|mailto|javascript|data:image\/[^;]{2,9};):/i.test(url))
            return url; //Url is already absolute

        var base_url = path; //location.href.match(/^(.+)\/?(?:#.+)?$/)[0]+"/";
        if(url.substring(0,2) == "//")
            return "pkx:" + url;
        else if(url.charAt(0) == "/")
            return base_url + url;
        else if(url.substring(0,2) == "./")
            url = url.substr(2);
        else if(/^\s*$/.test(url))
            return ""; //Empty = Return nothing
        //else url = url;

        url = base_url + url;
        var i=0
        while(/\/\.\.\//.test(url = url.replace(/[^\/]+\/+\.\.\//g,"")));

        /* Escape certain characters to prevent XSS */
        url = url.replace(/\.$/,"").replace(/\/\./g,"").replace(/"/g,"%22")
                .replace(/'/g,"%27").replace(/</g,"%3C").replace(/>/g,"%3E");
        return url;
    }
    function replaceAllRelativeURIs(isCSS, html, returnArray, path, p) {
        var uris = returnArray? {}   : path;
        path =     returnArray? path : p;

        /*HTML/XML Attribute may not be prefixed by these characters (common 
        attribute chars.  This list is not complete, but will be sufficient
        for this function (see http://www.w3.org/TR/REC-xml/#NT-NameChar). */
        var att = "[^-a-z0-9:._]";

        var entityEnd = "(?:;|(?!\\d))";
        var ents = {" ":"(?:\\s|&nbsp;?|&#0*32"+entityEnd+"|&#x0*20"+entityEnd+")",
                    "(":"(?:\\(|&#0*40"+entityEnd+"|&#x0*28"+entityEnd+")",
                    ")":"(?:\\)|&#0*41"+entityEnd+"|&#x0*29"+entityEnd+")",
                    ".":"(?:\\.|&#0*46"+entityEnd+"|&#x0*2e"+entityEnd+")"};
                    /* Placeholders to filter obfuscations */
        var charMap = {};
        var s = ents[" "]+"*"; //Short-hand for common use
        var any = "(?:[^>\"']*(?:\"[^\"]*\"|'[^']*'))*?[^>]*";
        /* ^ Important: Must be pre- and postfixed by < and >.
        *   This RE should match anything within a tag!  */

        /*
        @name ae
        @description  Converts a given string in a sequence of the original
                        input and the HTML entity
        @param String string  String to convert
        */
        function ae(string){
            var all_chars_lowercase = string.toLowerCase();
            if(ents[string]) return ents[string];
            var all_chars_uppercase = string.toUpperCase();
            var RE_res = "";
            for(var i=0; i<string.length; i++){
                var char_lowercase = all_chars_lowercase.charAt(i);
                if(charMap[char_lowercase]){
                    RE_res += charMap[char_lowercase];
                    continue;
                }
                var char_uppercase = all_chars_uppercase.charAt(i);
                var RE_sub = [char_lowercase];
                RE_sub.push("&#0*" + char_lowercase.charCodeAt(0) + entityEnd);
                RE_sub.push("&#x0*" + char_lowercase.charCodeAt(0).toString(16) + entityEnd);
                if(char_lowercase != char_uppercase){
                    /* Note: RE ignorecase flag has already been activated */
                    RE_sub.push("&#0*" + char_uppercase.charCodeAt(0) + entityEnd);   
                    RE_sub.push("&#x0*" + char_uppercase.charCodeAt(0).toString(16) + entityEnd);
                }
                RE_sub = "(?:" + RE_sub.join("|") + ")";
                RE_res += (charMap[char_lowercase] = RE_sub);
            }
            return(ents[string] = RE_res);
        }

        /*
        @name by
        @description  2nd argument for replace().
        */
        function by(match, group1, group2, group3){
            var uri = relativeURItoAbsolute(group2, path);
            if (returnArray) {
                uris[uri] = null;
            }
            else {
                uri = uris[uri];
            }
            /* Note that this function can also be used to remove links:
            * return group1 + "javascript://" + group3; */
            return group1 + uri + group3;
        }
        /*
        @name by2
        @description  2nd argument for replace(). Parses relevant HTML entities
        */
        var slashRE = new RegExp(ae("/"), 'g');
        var dotRE = new RegExp(ae("."), 'g');
        function by2(match, group1, group2, group3){
            /*Note that this function can also be used to remove links:
            * return group1 + "javascript://" + group3; */
            group2 = group2.replace(slashRE, "/").replace(dotRE, ".");

            var uri = relativeURItoAbsolute(group2, path);
            if (returnArray) {
                uris[uri] = null;
            }
            else {
                uri = uris[uri];
            }

            return group1 + uri + group3;
        }
        /*
        @name cr
        @description            Selects a HTML element and performs a
                                    search-and-replace on attributes
        @param String selector  HTML substring to match
        @param String attribute RegExp-escaped; HTML element attribute to match
        @param String marker    Optional RegExp-escaped; marks the prefix
        @param String delimiter Optional RegExp escaped; non-quote delimiters
        @param String end       Optional RegExp-escaped; forces the match to end
                                before an occurence of <end>
        */
        function cr(selector, attribute, marker, delimiter, end){
            if(typeof selector == "string") selector = new RegExp(selector, "gi");
            attribute = att + attribute;
            marker = typeof marker == "string" ? marker : "\\s*=\\s*";
            delimiter = typeof delimiter == "string" ? delimiter : "";
            end = typeof end == "string" ? "?)("+end : ")(";
            var re1 = new RegExp('('+attribute+marker+'")([^"'+delimiter+']+'+end+')', 'gi');
            var re2 = new RegExp("("+attribute+marker+"')([^'"+delimiter+"]+"+end+")", 'gi');
            var re3 = new RegExp('('+attribute+marker+')([^"\'][^\\s>'+delimiter+']*'+end+')', 'gi');
            html = html.replace(selector, function(match){
                return match.replace(re1, by).replace(re2, by).replace(re3, by);
            });
        }
        /* 
        @name cri
        @description            Selects an attribute of a HTML element, and
                                    performs a search-and-replace on certain values
        @param String selector  HTML element to match
        @param String attribute RegExp-escaped; HTML element attribute to match
        @param String front     RegExp-escaped; attribute value, prefix to match
        @param String flags     Optional RegExp flags, default "gi"
        @param String delimiter Optional RegExp-escaped; non-quote delimiters
        @param String end       Optional RegExp-escaped; forces the match to end
                                    before an occurence of <end>
        */
        function cri(selector, attribute, front, flags, delimiter, end){
            if(typeof selector == "string") selector = new RegExp(selector, "gi");
            attribute = att + attribute;
            flags = typeof flags == "string" ? flags : "gi";
            var re1 = new RegExp('('+attribute+'\\s*=\\s*")([^"]*)', 'gi');
            var re2 = new RegExp("("+attribute+"\\s*=\\s*')([^']+)", 'gi');
            var at1 = new RegExp('('+front+')([^"]+)(")', flags);
            var at2 = new RegExp("("+front+")([^']+)(')", flags);
            if(typeof delimiter == "string"){
                end = typeof end == "string" ? end : "";
                var at3 = new RegExp("("+front+")([^\"'][^"+delimiter+"]*" + (end?"?)("+end+")":")()"), flags);
                var handleAttr = function(match, g1, g2){return g1+g2.replace(at1, by2).replace(at2, by2).replace(at3, by2)};
            } else {
                var handleAttr = function(match, g1, g2){return g1+g2.replace(at1, by2).replace(at2, by2)};
        }
            html = html.replace(selector, function(match){
                return match.replace(re1, handleAttr).replace(re2, handleAttr);
            });
        }

        if (isCSS) {
            cr(/(?:[^"']*(?:"[^"]*"|'[^']*'))*?[^'"]*/gi, "url", "\\s*\\(\\s*", "", "\\s*\\)");
            cr(/(?:[^"']*(?:"[^"]*"|'[^']*'))*?[^"]*/gi, "url", "\\s*\\('\\s*", "", "\\s*'\\)");
        }
        else {
            /* <meta http-equiv=refresh content="  ; url= " > */
            cri("<meta"+any+att+"http-equiv\\s*=\\s*(?:\""+ae("refresh")+"\""+any+">|'"+ae("refresh")+"'"+any+">|"+ae("refresh")+"(?:"+ae(" ")+any+">|>))", "content", ae("url")+s+ae("=")+s, "i");

            cr("<[^a]"+any+att+"href\\s*="+any+">", "href"); /* Linked elements */ //added [^a] to disable page navigation links (to prevent in infinite loop during parsing if there are backlinks)
            cr("<"+any+att+"src\\s*="+any+">", "src"); /* Embedded elements */

            cr("<object"+any+att+"data\\s*="+any+">", "data"); /* <object data= > */
            cr("<applet"+any+att+"codebase\\s*="+any+">", "codebase"); /* <applet codebase= > */

            /* <param name=movie value= >*/
            cr("<param"+any+att+"name\\s*=\\s*(?:\""+ae("movie")+"\""+any+">|'"+ae("movie")+"'"+any+">|"+ae("movie")+"(?:"+ae(" ")+any+">|>))", "value");

            cr(/<style[^>]*>(?:[^"']*(?:"[^"]*"|'[^']*'))*?[^'"]*(?:<\/style|$)/gi, "url", "\\s*\\(\\s*", "", "\\s*\\)"); /* <style> */
            cr(/<style[^>]*>(?:[^"']*(?:"[^"]*"|'[^']*'))*?[^"]*(?:<\/style|$)/gi, "url", "\\s*\\('\\s*", "", "\\s*'\\)"); /* <style> */
            cri("<"+any+att+"style\\s*="+any+">", "style", ae("url")+s+ae("(")+s, 0, s+ae(")"), ae(")")); /*< style=" url(...) " > */
        }

        return returnArray? uris : html;
    }
    
    function loader(request, handler) {
        var selector;
        var pkxVolume;
        var mounting = {};
        var waiters = [];
        var requests = []; //dependencies
        var options;
    
        var resolve;
        var reject;
    
        function handleRequest(callback, fail) {
            resolve = callback;
            fail = reject;
    
            // check target
            try {
                validate(selector.target, object)
                    .when(object.compare, host);
            }
            catch(e) {
                if (selector.optional) {
                    // gracefully stop
                    resolve(null, true);
                    return;
                }
                else {
                    throw new Error(ERROR_TARGET_MISMATCH, "Target does not match, and the request is not optional.", selector.target);
                }
            }
    
            // change relative paths for embedded resources
            if (selector.package.substr(0, 2) == "./" && handler && handler.context) {
                selector.package = "pkx:///" + handler.context.id.substr(0, handler.context.id.indexOf("/")) + (selector.package.length > 2 ? "/" + selector.package.substr(2) : "");
            }
    
            // send the request off to the processors (ex. url replacement, variable substitution, ...)
            // if the processors are already processing a similar selector, it will not be processed again,
            // but rather wait until it is doen.
            for (var p in processors) {
                var promise = processors[p](selector);
                if (!promise) {
                    continue;
                }
                if (processing[selector.package]) {
                    // subscribe to existing processor
                    processing[selector.package].addEventListener("ready", function(a) { 
                        // the selector uri could be updated, so propagate the update
                        selector.uri = processing[selector.package].selector.uri;

                        if (a instanceof define.Module) {
                            resolve(a);
                        }
                        else {
                            fetchModule(a);
                        }
                    });
                    processing[selector.package].addEventListener("error", function(e) { 
                        error(e);
                    } );
                    return;
                }
    
                // create new event emitter
                processing[selector.package] = new event.Emitter(promise);
                processing[selector.package].selector = selector;
                
                function createReadyCallback(id) {
                    return function(a) { 
                        processing[id].fire("ready", a);
                        delete processing[id]; 

                        if (a instanceof define.Module) {
                            resolve(a);
                        }
                        else {
                            fetchModule(a);
                        }
                    }
                }
                function createErrorCallback(id) {
                    return function(e) { 
                        processing[id].fire("error", e);
                        error(e);
                    }
                }
    
                promise
                    .then(createReadyCallback(selector.package))
                    .catch(createErrorCallback(selector.package));
                return;
            }
    
            // no processor was found to prepare the request
            fetchModule();
        }
        function fetchModule(o) {
            options = o;

            // check define cache for existing module
            if (typeof define != "undefined" && define.using && !selector.raw) {
                var cached = define.cache.get(selector.id, selector.upgradable);
                if (cached) {
                    completeRequest(cached);
                    return;
                }
            }
    
            // if scheme is pkx, then get the mounted volume for the cache
            if (selector.uri.scheme == "pkx") {
                var selectorId = selector.uri.path.substr(1);
                    selectorId = selectorId.substr(0, selectorId.indexOf("/"));
                for (var v in volumes) {
                    if (selector.uri.path && volumes[v].pkx.id == selectorId) {
                        pkxVolume = volumes[v];
    
                        getDependencies();
                        return;
                    }
                }
            }
    
            // get existing volume for package
            pkxVolume = findVolume(selector);
    
            // if the volume was already mounted, proceed
            if (pkxVolume) {
                getDependencies();
                return;
            }
    
            // if the volume is already mounting, wait for the mounting to finish
            if (mounting[selector.id]) {
                mounting[selector.id].waitFor(function(volume) {
                    pkxVolume = volume;
                    getDependencies();
                });
                return;
            }
    
            // add waiting mechanism for other requests for this volume (to avoid mulitple mounting of the same volume)
            mounting[selector.id] = {
                "callbacks" : [],
                "waitFor" : function(callback) {
                    this.callbacks.push(callback);
                },
                "ready" : function(volume) {
                    while(this.callbacks.length > 0) {
                        this.callbacks[0](volume);
                        this.callbacks.splice(0, 1);
                    }
                    delete mounting[selector.id];
                }
            };
    
            // mount the pkx volume
            mountVolume(selector.uri, options).then(function (volume) {
                pkxVolume = volume;
    
                // execute all callbacks witing for this volume
                mounting[selector.id].ready(pkxVolume);

                getDependencies();
            }, error);
    
        }
        function getDependencies() {
            var pkxDeps = pkxVolume.pkx.pkx && pkxVolume.pkx.pkx.dependencies? pkxVolume.pkx.pkx.dependencies : pkxVolume.pkx.dependencies;
            if (selector.raw && selector.ignoreDependencies) {
                getResourceStreamFromVolume();
                return;
            }
            else {
                for (var d in pkxDeps) {
                    var embedded;

                    //TODO - I WAS HERE -> MAKE SURE THE NODE DEPENDENCY SYSTEM (WITH ALIAS) IS SUPPORTED
                    switch (type.getType(pkxDeps[d])) {
                        case type.TYPE_OBJECT:
                            if (pkxDeps[d].system &&
                                pkxDeps[d].system != PKX_SYSTEM) {
                                requests[d] = pkxDeps[d];
                            }
                        // fallthrough intended
                        case type.TYPE_STRING:
                            // modify relative uri for embedded packages
                            if (pkxDeps[d].substr(0, 2) == "./") {
                                var r = pkxDeps[d];
                                var p = "pkx:///" + (options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id + "/";
                                if (r.length > 2) {
                                    r = r.substr(2);
                                }
                                embedded = true;

                                requests[d] = new PKXSelector(p);
                            }
                            else {
                                requests[d] = new PKXSelector(pkxDeps[d]);
                            }

                            if (embedded) {
                                requests[d].package = p;
                                requests[d].resource = r;
                            }

                            if (type.getType(pkxDeps) == type.TYPE_OBJECT) {
                                requests[d].alias = d;
                            }
                            if (selector.raw) {
                                requests[d].wrap = selector.wrap;
                                requests[d].raw = true;
                            }
                            break;
                        default:
                            // unknown system
                            requests[d] = pkxDeps[d];
                    }

                    // add options from parent request if not set
                    requests[d].options = requests[d].options || selector.options;
    
                    if (requests[d].wrap) {
                        requests[d].ignoreCache = true;
                    }
    
                    // skip circular, embedded dependencies
                    var selUri = selector.uri.toString();
                    selUri = selUri.substr(0, selUri.indexOf("/",7)) || selector.uri.toString();
                    if (embedded && selector.uri.scheme == "pkx" && selUri == "pkx:///" + (options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id) {
                        requests[d] = null;
                    }
                }
            }
    
            // filter out removed dependencies
            var filtered = [];
            for (var r in requests) {
                if (requests[r]) {
                    filtered.push(requests[r]);
                }
            }
            requests = filtered;
    
            // require all dependencies
            using.apply(this, requests).then(getResourceStreamFromVolume, function(loader) {
                var halt;
                var mods = [];
                for (var r in loader.requests) {
                    if (loader.requests[r].err.length > 0 && !loader.requests[r].request.optional) {
                        halt = true;
                    }
                    else {
                        mods.push(loader.requests[r].module);
                    }
                }
                if (!halt) {                                  
                    getResourceFrompkxVolume.apply(this, mods);
                    return;
                }
                error(new Error(ERROR_DEPENDENCY, "", loader));
            }, true);
        }
        function getResourceStreamFromVolume() {
            // find out which resource to load
            var resource = null;
            if (selector.resource) {
                resource = selector.resource;
            }
            else {
                var main = pkxVolume.pkx.pkx && pkxVolume.pkx.pkx.main? pkxVolume.pkx.pkx.main : pkxVolume.pkx.main;
                // get first matching main (by target)
                if (type.isString(main)) {
                    resource = main;
                }
                else if (main) {
                    for (var m in main) {
                        if(main[m].target) {
                            try {
                                validate(main[m].target, object)
                                    .when(object.compare, host);
                                resource = main[m].resource;
                            }
                            catch(e) {
                                // ignore for now
                            }
                        }
                    }
                }
            }
    
            if (!resource) {
                error(new Error(ERROR_NO_ENTRY_POINT, "Package '" + pkxVolume.pkx.id + "' does not have an entry point defined for the current target."));
                return;
            }
    
            // prepend slash to resource name if not present
            if (resource.substr(0,1) != "/") {
                resource = "/" + resource;
            }
    
            var dependencies = []; // I WAS HERE -> MAYBE ADD SOME KIND OF OBJECT THAT HOLD MODULE AND ALIAS (BECAUSE MODULE IS SHARED, AND ALIAS IS NOT
            for (var a=0;a<arguments.length;a++) {
                // dependency module
                dependencies[a] = arguments[a];
                if (requests[a].alias) {
                    dependencies[requests[a].alias] = dependencies[a];
                }
            }
    
            if (!requested[(options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id + resource] && !selector.raw) {
                requested[(options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id + resource] = true;
            }
            else if (!selector.raw) {
                define.cache.waitFor((options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id + (selector.resource || pkxVolume.pkx.id.substr(pkxVolume.pkx.id.length - 1) == "/"? selector.resource : "/"), completeRequest);
                return;
            }
    
            pkxVolume.open(resource).then(function readDataFromResourceStream(stream) {
                if (selector.raw && !selector.wrap) {
                    completeRequest(stream, dependencies);
                    return;
                }
                stream.readAsString().then(function processCode(data) {
                    var name = stream.getName();
                    var ext = name.substr(stream.getName().lastIndexOf(".") + 1);
                    var raw = ext != "js" && ext != "json" && ((ext != "css" && ext == "html" && ext == "htm") || ((ext == "css" || ext == "html" || ext == "htm") && !host.isRuntimeBrowserFamily() && host.runtime != host.RUNTIME_NWJS));
    
                    // wrap code for define
                    if (ext == "js") {
                        data = loader.wrap(data, pkxVolume.pkx, selector.resource, (pkxVolume.pkx.pkx && pkxVolume.pkx.pkx.dependencies? pkxVolume.pkx.pkx.dependencies : pkxVolume.pkx.dependencies), selector.configuration, (host.runtime != host.RUNTIME_NODEJS), (options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id + resource, (selector.raw && selector.wrap), (options && options.ns? options.ns : null));
                    }
    
                    if (selector.raw || raw) {
                        completeRequest(new ioBufferedStream(data.toUint8Array()), dependencies);
                        return;
                    }
    
                    // add dependencies
                    define.parameters = {};
                    define.parameters.wrapped = false;
                    define.parameters.system = PKX_SYSTEM;
                    define.parameters.id = (options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id + (selector.resource || pkxVolume.pkx.id.substr(pkxVolume.pkx.id.length - 1) == "/"? selector.resource : "/");
                    define.parameters.pkx = pkxVolume.pkx;
                    define.parameters.dependencies = [ DEPENDENCY_PKX, "module", DEPENDENCY_CONFIG, DEPENDENCY_REQUIRER ];
                    define.parameters.dependencies[0] = pkxVolume.pkx;
                    for(var d in dependencies) {
                        if (isNaN(d)) {
                            define.parameters.dependencies[d + (selector.resource || pkxVolume.pkx.id.substr(pkxVolume.pkx.id.length - 1) == "/"? selector.resource : "/")] = dependencies[d];
                            continue;
                        }
                        define.parameters.dependencies.push(dependencies[d]);
                    }
    
                    if (ext == "js") {
                        // load code
                        if (host.isRuntimeNodeFamily() && !host.isRuntimeBrowserFamily()) {
                            try {
                                vm = require("vm");
                                global.require = global.require || require;
                                vm.runInThisContext(data, {filename: (selector.uri.scheme == "file"? process.cwd() : "") + resource, lineOffset: -1});
                            }
                            catch (e) {
                                error(e);
                                return;
                            }
                            completeRequest(null, null, true);
                        }
                        else if (host.isRuntimeBrowserFamily()) {
                            var script = document.createElement("script");
                            script.language = "javascript";
                            script.type = "text/javascript";
                            script.text = data;
                            try {
                                document.body.appendChild(script);
                            }
                            catch (e) {
                                error(e);
                                return;
                            }
                            completeRequest(null, null, true);
                        }
                        else {
                            error(new Error("Loading code in runtime '" + host.runtime + "' is not supported."));
                        }
                    }
                    if (ext == "json") {
                        var json;
                        try {
                            json = JSON.parse(data);
                        }
                        catch (e) {
                            error(e);
                            return;
                        }
                        completeRequest(json, dependencies);
                    }
                    if (ext == "css") {
                        if (typeof document !== "undefined") {
                            data += "\r\n/*# sourceURL=http://" + ((options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id + resource) + "*/";
                            var style = document.createElement("style");
                            style.rel = "stylesheet";
                            style.type = "text/css";
                            if (style.styleSheet) {
                                style.styleSheet.cssText = data;
                            }
                            else {
                                style.appendChild(document.createTextNode(data));
                            }
                            try {
                                document.head.appendChild(style);
                            }
                            catch (e) {
                                error(e);
                                return;
                            }
                        }
                        completeRequest(data, dependencies);
                    }
                    if (ext == "html" || ext == "htm") {
                        if (typeof document !== "undefined") {
                            // replace relative uris
                            //var resPath = PROTOCOL_PKX + ":///" + define.parameters.id + resource.substr(0, resource.lastIndexOf("/"));
                            //replaceURIsInHTML(resPath, data).then(function(html) {
                                // create new iframe
                                var iframe = document.createElement("iframe");
                                // iframe.src = "data:text/html;charset=utf-8," + encodeURI(html); // USING THIS METHOD BREAKS THE RESOURCE LOADING USING BLOB URLS
                                iframe.setAttribute("data-pkx-app-id", define.parameters.id);
                                // set app mode for applying css to body and iframe
                                document.documentElement.setAttribute("data-pkx-mode", "app");

                                try {
                                    document.body.appendChild(iframe);
                                    iframe.contentDocument.write(data);
                                }
                                catch (e) {
                                    error(e);
                                    return;
                                }
                                completeRequest(data, dependencies);
                            //}).catch(error);
                        }
                        else {
                            error(new Error(ERROR_UNSUPPORTED_OPERATION, "The runtime does not support loading html files."));
                        }
                    }
                }, error);
            }, error);
        }
        function error(e) {
            loaderObj.err.push(e);
    
            if (!loaderObj.module && pkxVolume) {
                loaderObj.module = new define.Module();
                loaderObj.module.id = (pkxVolume.pkx? pkxVolume.pkx.id : (request.package? request.package : request)) + (request.resource || (pkxVolume.pkx && pkxVolume.pkx.id.substr(pkxVolume.pkx.id.length - 1) == "/")? request.resource : "/");
                if (pkxVolume.pkx) {
                    loaderObj.module.parameters = {
                        "id":     pkxVolume.pkx.id + (request.resource ? request.resource : "/"),
                        "system": PKX_SYSTEM,
                        "pkx":    pkxVolume.pkx
                    }
                }
            }
    
            resolve();
            return;
        }
        function completeRequest(fact, dependencies, checkWait) {
            // check for delayed loading
            if (checkWait && define.parameters.wait) {
                waiters =  define.parameters.wait;
    
                for (var w in waiters) {
                    waiters[w].then(completeRequest, error);
                }
                return;
            }
            else {
                // wait for all waiters to complete
                var allDone = true;
                for (var w in waiters) {
                    if (!waiters[w].done) {
                        allDone = false;
                        break;
                    }
                }
                if (!allDone) {
                    return;
                }
            }
    
            if (!fact) {
                fact = define.cache.get((options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id + (request.resource? request.resource : "/"));
            }
            else if (!(fact instanceof define.Module)) {
                var mod = new define.Module();
                mod.id = (options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id + (selector.resource || pkxVolume.pkx.id.substr(pkxVolume.pkx.id.length - 1) == "/"? selector.resource : "/");
                mod.factory = fact;
                mod.dependencies = dependencies || [];
                mod.parameters = {
                    "id" : (options && options.ns? options.ns + "." : "") + pkxVolume.pkx.id + (request.resource? request.resource : "/"),
                    "system" : PKX_SYSTEM,
                    "pkx" : pkxVolume.pkx
                };
                fact = mod;
            }
    
            resolve(fact);
            return;
        }
    
        // validate request
        try {
            selector = new PKXSelector(request);
        }
        catch(e) {
            if (e instanceof Error && e.name == ERROR_INVALID_PKX_SELECTOR) {
                throw new RangeError(e.message);
            }
            else {
                throw e;
            }
        }
    
        // return a loader
        var loaderObj = new define.Loader(request, handleRequest);
        return loaderObj;
    }
    loader.factory = function(module, factory, request, requirer) {
        // decorate dependencies
        var dependencies = module.dependencies.slice(0);
        var args = [];

        // Replace the pkx descriptor or pkx placeholder with an instance of PKX, unless type, string and validate
        // are not loaded yet. This means that when they are first instantiated by this module, they will not have
        // the functions available to a PKX instance. This is not a disaster since those base libraries do not use
        // them. Any instances created after the initial load will have them available, except if they're singleton.
        for (var d in dependencies) {
            if ((dependencies[d] === module.parameters.pkx ||
                !isNaN(d) && dependencies[d] == DEPENDENCY_PKX) &&
                type && string && object && array && boolean && validate) {
                dependencies[d] = new PKX(dependencies[d] == DEPENDENCY_PKX? module.parameters.pkx : dependencies[d]);
            }
            if (!isNaN(d) && dependencies[d] == DEPENDENCY_CONFIG) {
                dependencies[d] = request && request.configuration? request.configuration : (module.parameters.configuration? module.parameters.configuration : {});
            }
            if (!isNaN(d) && dependencies[d] == DEPENDENCY_REQUIRER) {
                dependencies[d] = requirer;
            }
        }

        // add other dependencies (only numbered index)
        for (var a=0;a<dependencies.length;a++) {
            args.push(dependencies[a]);
        }
    
        // execute module factory
        return factory.apply(factory, args);
    };
    loader.wrap = function(code, pkx, resourceId, dependencies, configuration, addSourceMap, sourceMapName, isEmbedded, ns) {
        if (!resourceId) {
            resourceId = "/";
        }
    
        var depStr = "";
    
        if (isEmbedded) {
        }
    
        // stringify the package definition (pretty print)
        var descriptor = JSON.stringify(pkx, null, Array(INDENT_OFFSET + 1).join(" "));
    
        // generate module definition code
        var moduleCode = "(function(module, using, require) {\n";
        if (isEmbedded) {
            // add configuration
            configuration = configuration? "\ndefine.parameters.configuration = " + JSON.stringify(configuration, null, Array(INDENT_OFFSET + 1).join(" ")) + ";" : "";
    
            // add package dependency
            depStr += "\ndefine.parameters.dependencies = [ \"" + DEPENDENCY_PKX + "\", \"module\", \"" + DEPENDENCY_CONFIG + "\", \"" + DEPENDENCY_REQUIRER + "\" ];";
            depStr += "\ndefine.parameters.dependencies[0] = define.parameters.pkx;";
    
            if (dependencies) {
                for (var t = 0; t < dependencies.length; t++) {
                    var dep = dependencies[t];
                    if (dep) {
                        var resName = dep + "/";
                        if (Object.prototype.toString.call(dep) === "[object Object]") {
                            resName = dep.package + (dep.resource ? dep.resource : "/");
                        }
                        depStr += "\ndefine.parameters.dependencies.push(define.cache.get(\"" + (ns? ns + "." : "") + resName + "\"));";
                    }
                }
            }
    
            moduleCode += "define.parameters = {};\n" + (isEmbedded? "define.parameters.wrapped = true;\n" : "") + "define.parameters.system = \"" + PKX_SYSTEM + "\";\ndefine.parameters.id = \"" + (ns? ns + "." : "") + pkx.id + resourceId + "\";\ndefine.parameters.pkx = " + descriptor + ";" + depStr + configuration + "\n";
        }
        moduleCode += "define.prepare(" + (isEmbedded? "" : "define.parameters.id") + ");\n\n";
        moduleCode += "using = define.getUsing(define.parameters.id);\n";
        moduleCode += "require = define.getRequire(define.parameters.id, require);\n";
        moduleCode += code + "\n\nif(module.exports) {\n    define(function factory() { return module.exports; });\n}\n})({},typeof using != \"undefined\"? using : null, typeof require != \"undefined\"? require : null);" + (addSourceMap? "\n//# sourceURL=http://" + (sourceMapName? sourceMapName : ((ns? ns + "." : "") + pkx.id + resourceId)) : "");
    
        // add code indent to make it pretty
        var moduleLines = moduleCode.split(/\r*\n/);
        var indentWhiteSpace = Array(INDENT_OFFSET + 1).join(" ");
    
        // add header to make it pretty
        if (isEmbedded) {
            moduleCode = "/////////////////////////////////////////////////////////////////////////////////////\n";
            moduleCode += "//\n";
            moduleCode += "// module '" + pkx.id + resourceId + "'\n";
            moduleCode += "//\n";
            moduleCode += "/////////////////////////////////////////////////////////////////////////////////////\n";
            moduleCode += moduleLines[0] + "\n";
            for (var l=1;l<moduleLines.length - (addSourceMap? 2 : 1);l++) {
                moduleCode += indentWhiteSpace + moduleLines[l] + "\n";
            }
            if (addSourceMap) {
                moduleCode += moduleLines[moduleLines.length - 2] + "\n";
            }
            moduleCode += moduleLines[moduleLines.length - 1] + "\n";
        }
    
        return moduleCode;
    };
    loader.addRequestProcessor = function(name, fn) {
        if (processors[name]) {
            throw new Error(ERROR_INVALID_REQUEST_PROCESSOR, "A processor with name '" + name + "' is already registered.");
        }
        processors[name] = fn;
    };
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // PKXSelector Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function PKXSelector(selector, validateId) {
        var own = this;
        var errName = ERROR_INVALID_PKX_SELECTOR;
        var addDefaultResource = false;
        var valid = true; // indicates invalid naming pattern
        var majorVersion = null;
        var minorVersion = null;
        var patchVersion = null;
        var repository = null;
    
        switch(type.getType(selector)) {
            case type.TYPE_STRING:
                selector = { 
                    "package" : selector
                };
                addDefaultResource = true;
                break;
            case type.TYPE_OBJECT:
                break;
            default:
                throw new Error(errName, "Mandatory parameter 'selector' should be of type 'String' or 'Object'.");
        }

        validate(selector.package, string, errName, "package")
            .when(string.isValid);
    
        // verify naming pattern
        var idxSlash = selector.package.lastIndexOf("/", selector.package.length - 2);
        // when a slash is detected trim off pkx extension if present
        var parts = (idxSlash >= 0? selector.package.substring(idxSlash + 1, selector.package.lastIndexOf(PKX_FILE_EXTENSION) == selector.package.length - PKX_FILE_EXTENSION.length? selector.package.length - PKX_FILE_EXTENSION.length: null) : selector.package).split(".");
        if (parts.length > 0 && parts[parts.length - 1].substr(parts[parts.length - 1].length - 1) == "/") {
            parts[parts.length - 1] = parts[parts.length - 1].substr(0,parts[parts.length - 1].length - 1);
        }
        // if package does not contain any slashes, assume it is an id
        if (selector.package.indexOf("/") == -1) {
            if (!isNaN(parts[parts.length - 1]) && !isNaN(parts[parts.length - 3])) {
                patchVersion = isNaN(parts[parts.length - 1]);
            }
            minorVersion = parts[parts.length - (isNaN(parts[parts.length - 3]) ? 1 : 2)];
            if (isNaN(minorVersion)) {
                minorVersion = null;
                valid = false;
            }
            majorVersion = parts[parts.length - (isNaN(parts[parts.length - 3]) ? 2 : 3)];
            if (isNaN(majorVersion)) {
                majorVersion = null;
                valid = false;
            }
        }
        if (validateId && !valid) {
            throw new Error(errName, "Mandatory property 'package' does not match the " + PKX_SYSTEM + " naming pattern.");
        }
        validate(selector.resource, string, errName)
            .when(string.isPath);
        validate(selector.target, object, errName);
        validate(selector.raw, boolean, errName);
        validate(selector.wrap, boolean, errName);
        validate(selector.system, string, errName);
        validate(selector.optional, boolean, errName);
        validate(selector.alias, string, errName);
    
        // normalise path (add leading slash)
        if (selector.resource && selector.resource.length > 0 && selector.resource.substr(0,1) != "/") {
            selector.resource = "/" + selector.resource;
        }
    
        var id;
        var uri;
        var vers = {};
        Object.defineProperty(vers, "major", {
            get: function() {
                return majorVersion;
            }
        });
        Object.defineProperty(vers, "minor", {
            get: function() {
                return minorVersion;
            }
        });
        Object.defineProperty(vers, "patch", {
            get: function() {
                return patchVersion;
            }
        });
        Object.defineProperty(this, "version", {
            get: function() {
                return vers;
            }
        });
        Object.defineProperty(this, "id", {
            set: function(value) {
                id = value;
            },
            get: function() {
                return id || (own.package + (addDefaultResource? "/" : (own.resource || "")));
            }
        });
        Object.defineProperty(this, "name", {
            get: function() {
                // get full name (strip version numbers from id string)
                var name = "";
                var nameParts = own.package.split(".");
                for (var i = 0; i < nameParts.length; i++) {
                    if (isNaN(nameParts[i])) {
                        name += (name != "" ? "." : "") + nameParts[i];
                    }
                }
                return name;
            }
        });
        Object.defineProperty(this, "repository", {
            get: function() {
                return repository;
            }
        });
        Object.defineProperty(this, "uri", {
            get: function() {
                return uri;
            },
            set: function(u) {
                if (typeof u === "string") {
                    uri = ioURI.parse(replaceVariables(u));
                }
                else if ((u instanceof ioURI)) {
                    uri = u;
                }
                else {
                    throw new Error("The URI should be of type 'String' or 'URI'.");
                }
            }
        });
        Object.defineProperty(this, "isArchive", {
            get: function() {
                return own.package.lastIndexOf("/") != own.package.length - 1;
            }
        });
        this.alias = selector.alias;
        this.package = selector.package;
        this.resource = selector.resource;
        this.target = selector.target;
        this.raw = selector.raw || false;
        this.wrap = selector.wrap || false;
        this.system = selector.system;
        this.optional = selector.optional || false;
        this.upgradable = selector.upgradable || (using? using.UPGRADABLE_NONE : null);
        this.ignoreDependencies = selector.ignoreDependencies || false;
        this.configuration = selector.configuration || null;
        this.options = selector.options || null;

        uri = ioURI.parse(replaceVariables(selector.package));
    
        this.parseURI = function(u, namespaceSeperator) {
            return ioURI.parse(replaceVariables(u, namespaceSeperator));
        };
    
        function replaceVariables(u, namespaceSeperator) {
            // add .pkx extension
            var uriPKXName = (namespaceSeperator? own.package.replace(/\./g,namespaceSeperator) : own.package) + (own.package.lastIndexOf(PKX_FILE_EXTENSION) != own.package.length - PKX_FILE_EXTENSION.length && own.isArchive ? PKX_FILE_EXTENSION : "");
            // replace variables
            return u.replace(/\$NAME_NO_NS/g, (namespaceSeperator? own.name.replace(/\./g,namespaceSeperator) : own.name).substr(own.repository && own.repository.namespace && own.repository.namespace.length > 0? own.repository.namespace.length + 1 : 0))
                .replace(/\$NAME/g, (namespaceSeperator? own.name.replace(/\./g,namespaceSeperator) : own.name))
                .replace(/\$PATCH/g, patchVersion)
                .replace(/\$MINOR/g, minorVersion)
                .replace(/\$MAJOR/g, majorVersion)
                .replace(/\$PACKAGE/g, uriPKXName)
                .replace(/\$ID/g, (namespaceSeperator? own.package.replace(/\./g,namespaceSeperator) : own.package));
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // PKX Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function PKX(descriptor){
        var own = this;
        var errName = ERROR_INVALID_PKX_DESCRIPTOR;
    
        switch(type.getType(descriptor)) {
            case type.TYPE_STRING:
                try {
                    descriptor = JSON.parse(descriptor);
                }
                catch(e) {
                    throw new Error(errName, "Could not parse the JSON string.", e);
                }
                break;
            case type.TYPE_OBJECT:
                break;
            default:
                throw new Error(errName, "Mandatory parameter 'descriptor' should be of type 'String' or 'Object'.");
        }
    
        var name = [ string.LOWERCASE_LETTER, string.DIGIT, string.DASH ];
        validate(descriptor.name, string, errName)
            .allow(name, string.DOT)
            .when(descriptor.name != "")
            .notNull();
        validate(descriptor.version, string, errName)
            .when(string.isSemVer)
            .notNull();
        validate(descriptor.title, string, errName);
        validate(descriptor.description, string, errName);
        validate(descriptor.pkx, object, errName)
        //    .notNull();
    
        // merge all descriptor properties
        own = merge(descriptor, this);
    
        Object.defineProperty(own, "id", {
            get : function() {
                return own.name + "." + own.version;
            }
        });
    
        // return modified object
        return own;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // PKXVolume Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function PKXVolume(volume, pkx) {
        // validate arguments
        if (!(volume instanceof tar.TarVolume)) {
            throw new TypeError("Mandatory parameter 'volume' should be of type 'TarVolume'.");
        }
        if (!(pkx instanceof PKX)) {
            throw new TypeError("Mandatory parameter 'pkx' should be of type 'PKX'.");
        }
    
        this.id =          pkx.id + PKX_FILE_EXTENSION;
        this.name =        pkx.name;
    
        this.protocol =    PROTOCOL_PKX;
        this.description = "Package " + pkx.id;
        this.state =       ioVolume.STATE_INITIALIZING;
        this.size =        volume.size;
        this.type =        ioVolume.TYPE_REMOVABLE;
        this.scope =       ioVolume.SCOPE_LOCAL;
        this.class =       ioVolume.CLASS_TEMPORARY;
        this.readOnly =    true;
        this.pkx =         pkx;
    
        // bind to source volume functions
        //this.open =              volume.open?              Function.prototype.bind.call(volume.open,              volume) : this.open;
        this.open = function(uri) {
            return new Promise(function(resolve, reject) {
                return volume.open(uri).then(function(stream) {
                    resolve(new StreamPKX(stream, pkx.id, uri));
                }).catch(reject);
            });
        }
        this.delete =            volume.delete?            Function.prototype.bind.call(volume.delete,            volume) : this.delete;
        this.exists =            volume.exists?            Function.prototype.bind.call(volume.exists,            volume) : this.exists;
        this.query =             volume.query?             Function.prototype.bind.call(volume.query,             volume) : this.query;
        this.close =             volume.close?             Function.prototype.bind.call(volume.close,             volume) : this.close;
        this.getURI = function() {
            return new Promise(function(resolve, reject) {
                resolve(PROTOCOL_PKX + ":///" + pkx.id + "/");
            });
        }
    }
    PKXVolume.prototype = ioVolume;

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // StreamPKX Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function StreamPKX(stream, pkxId, resource) {
        var own = this;

        var resId =   PROTOCOL_PKX + ":///" + pkxId + (resource.substr(0, 1) !== "/" ? "/" : "") + resource;
        var resPath = resId.substr(0, resId.lastIndexOf("/") + 1);

        this.getName =   Function.prototype.bind.call(stream.getName,   stream);
        this.getLength = Function.prototype.bind.call(stream.getLength, stream);
        this.read = function (len, position) {
            if (!position) {
                position = 0;
            }

            //return stream.read(len, position);

            return new Promise(function (resolve, reject) {
                if (!len) {
                    return stream.getMimeType().then(function(mimeType) {
                        if (streamParseFileDone[resId]) {
                            resolve(streamParseFileDone[resId]);
                        }
                        else if (mimeType == "text/html" ||
                            mimeType == "text/css") {
                            // parse file html and css relative links
                            return stream.readAsString().then(function(data) {
                                // replace relative uris
                                // todo check mime-type and parse css or html if mime type match
                                return (mimeType == "text/css"? replaceURIsInCSS(resPath, data) : replaceURIsInHTML(resPath, data)).then(function(html) {
                                    streamParseFileDone[resId] = html;
                                    resolve(html);
                                }).catch(reject);
                            }).catch(reject);
                        }
                        else {
                            resolve(stream.read(len, position));
                        }
                    }).catch(reject);
                    
                }
                else {
                    resolve(stream.read(len, position));
                }
                //return stream.read(len, position);
            });
        };
        this.write =     Function.prototype.bind.call(stream.write,     stream);
        this.close =     Function.prototype.bind.call(stream.close,     stream);

        this.createObjectURL = function() {
            return new Promise(function(resolve, reject) {
                if (streamObjectURIs[resId]) {
                    if (type.isArray(streamObjectURIs[resId])) {
                        streamObjectURIs[resId].push(resolve);
                    }
                    else {
                        resolve(streamObjectURIs[resId]);
                    }
                }
                else {
                    streamObjectURIs[resId] = [];
                    return StreamPKX.prototype.createObjectURL.call(own).then(function(uri) {
                        var clbks = streamObjectURIs[resId];
                        streamObjectURIs[resId] = uri;

                        if (type.isArray(clbks)) {
                            for (var c in clbks) {
                                clbks[c](uri);
                            }
                        }
                        
                        resolve(uri);
                    }).catch(reject);
                }
            });
        };
    };
    StreamPKX.prototype = ioStream;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // URI Handler
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var mod = {};
    mod.parse = function(uri) {
        if(string.isURL(uri)) {
            if (uri.length >= 5 && uri.substr(0,5) == PROTOCOL_PKX + "://") {
                return new ioURI(uri, self);
            }
        }
    };
    mod.open = function(uri, opt_access) {
        if (uri && type.isString(uri)) {
            uri = ioURI.parse(uri);
        }
        else if (uri && (typeof uri.scheme== "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        var idxSlash = uri.path.indexOf("/", 1);
        var volId = uri.path? uri.path.substring(1, idxSlash > -1 ? idxSlash : undefined) : null;
        var resId = uri.path? uri.path.substr(volId.length + 1) : null;
        if (!volumes[volId]) {
            throw new Error(ERROR_INVALID_PKX_VOLUME, "PKXVolume '" + volId + "' is not mounted.");
        }
        return volumes[volId].open(resId, opt_access);
    };
    mod.exists = function(uri) {
        return new Promise(function(resolve, reject) {
            reject(new Error(ERROR_UNSUPPORTED_OPERATION, "The pkx module does not support querying files from pkx volumes."));
        });
    };
    mod.toString = function(uri) {
        return uri.toString();
    };
    mod.delete = function(uri) {
        return new Promise(function(resolve, reject) {
            reject(new Error(ERROR_UNSUPPORTED_OPERATION, "The pkx module does not support deleting files from pkx volumes."));
        });
    };
    mod.getTemp = function() {
        throw new Error(ERROR_UNSUPPORTED_OPERATION, "The pkx module does not support creating temp files.");
    };
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Register PKX Protocol Handler
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    ioURI.protocols.register(mod, PROTOCOL_PKX);
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Register PKX Extension
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    if (typeof define === "function" && define.cache && define.using) {
        try {
            define.Loader.register(PKX_SYSTEM, loader);
        }
        catch(e) {
            if (!(e instanceof RangeError)) {
                throw e;
            }
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = mod;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://pkx.1.0.0/pkx.js

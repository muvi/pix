/////////////////////////////////////////////////////////////////////////////////////
//
// module 'mime-type.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "mime-type.1.0.0/";
    define.parameters.pkx = {
        "name": "mime-type",
        "version": "1.0.0",
        "main": "mime-type.js",
        "dependencies": {
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "type": "https://gitlab.com/muvi/type#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "type": "https://gitlab.com/muvi/type#semver:^1.0"
            },
            "main": "mime-type.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("type.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // mime-type
    //
    //    Library that contains functions for mime-type detection.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var Error = require("error");
    var type  = require("type");
    
    var ERROR_INVALID_BUFFER =    "Invalid Buffer";
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Functions
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function MimeType(bytes, name) {
        //TODO - IMPLEMENT MORE TYPES, GOOD START BELOW
        //https://github.com/sindresorhus/file-type/blob/master/index.js
        //http://www.garykessler.net/library/file_sigs.html
    
        if (!type.isArray(bytes) && Object.prototype.toString.call(bytes) !== "[object Uint8Array]") {
            throw new Error(Error.ERROR_INVALID_PARAMETER, "Expected an array, instead of '" + type.getType(bytes) + "'.");
        }
        if (bytes.length < 16) {
            throw new Error(ERROR_INVALID_BUFFER, "Expected an array containing at least 16 bytes, but got an array containing " + bytes.length + " bytes.");
        }
    
        var header = "";
        for (var i = 0; i < bytes.length; i++) {
            header += bytes[i].toString(16);
        }
    
        switch (header) {
            case "89504e47":
                return "image/png";
            case "47494638":
                return "image/gif";
            case "ffd8ffe0":
            case "ffd8ffe1":
            case "ffd8ffe2":
                return "image/jpeg";
            default:
                // try file extension
                if (name) {
                    var ext = name.substr(name.lastIndexOf("."));
                    switch (ext) {
                        case ".css":
                            return "text/css";
                        case ".htm":
                        case ".html":
                            return "text/html";
                        case ".js":
                            return "text/javascript";
                        case ".svg":
                            return "image/svg+xml";
                        default:
                            return "unknown";
                    }
                }
                break;
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = MimeType;
    module.exports.ERROR_INVALID_BUFFER =  ERROR_INVALID_BUFFER;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://mime-type.1.0.0/mime-type.js

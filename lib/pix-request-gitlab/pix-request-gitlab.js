/////////////////////////////////////////////////////////////////////////////////////
//
// module 'pix-request-gitlab.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "pix-request-gitlab.1.0.0/";
    define.parameters.pkx = {
        "name": "pix-request-gitlab",
        "version": "1.0.0",
        "main": "pix-request-gitlab.js",
        "dependencies": {
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "host": "https://gitlab.com/muvi/host#semver:^1.0",
            "object-clone": "https://gitlab.com/muvi/object-clone#semver:^1.0",
            "version": "https://gitlab.com/muvi/version#semver:^1.0",
            "string-validation": "https://gitlab.com/muvi/string-validation#semver:^1.0",
            "config": "https://gitlab.com/muvi/config#semver:^1.0",
            "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
            "io-access": "https://gitlab.com/muvi/io-access#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "host": "https://gitlab.com/muvi/host#semver:^1.0",
                "object-clone": "https://gitlab.com/muvi/object-clone#semver:^1.0",
                "version": "https://gitlab.com/muvi/version#semver:^1.0",
                "string-validation": "https://gitlab.com/muvi/string-validation#semver:^1.0",
                "config": "https://gitlab.com/muvi/config#semver:^1.0",
                "io-uri": "https://gitlab.com/muvi/io-uri#semver:^1.0",
                "io-access": "https://gitlab.com/muvi/io-access#semver:^1.0"
            },
            "main": "pix-request-gitlab.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("host.1.0/"));
    define.parameters.dependencies.push(define.cache.get("object-clone.1.0/"));
    define.parameters.dependencies.push(define.cache.get("version.1.0/"));
    define.parameters.dependencies.push(define.cache.get("string-validation.1.0/"));
    define.parameters.dependencies.push(define.cache.get("config.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-uri.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-access.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // pix-request-gitlab
    //
    //    Request module for fetching releases from GitLab.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    //
    // Designed to accept the following requests:
    //    git+https://gitlab.com/muvi/pix-request-gitlab.git#semver:^1.0
    //    https://gitlab.com/muvi/pix-request-gitlab.git#semver:^1.0
    //    https://gitlab.com/muvi/pix-request-gitlabt#semver:^1.0
    //    https://gitlab.com/muvi/pix-request-gitlab
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    //
    // Privates
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
        Error =    require("error");
    var clone =    require("object-clone");
    var host =     require("host");
    var version =  require("version");
    var string =   require("string-validation");
    var config =   require("config");
    var ioURI =    require("io-uri");
    var ioAccess = require("io-access");
    
    var REQUEST_PROC_NAME =                  "gitlab";
    var HOST_GITLAB =                        "gitlab.com";
    var PATH_CACHE =                         "pix-request-gitlab/cache/";
    var EXT_PKX =                            "pkx";
    var HTTP_HEADERS =                       host.isRuntimeBrowserFamily()? {} : { "user-agent" : "pix" };
    
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    //
    // Functions
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    function request(selector) {
        if (selector.uri.authority.host != HOST_GITLAB) {
            return;
        }
    
        
        return new Promise(function (resolve, reject) {
            // remove git+ from protocol
            selector.uri.scheme = selector.uri.scheme.substr(0,4) === "git+"? selector.uri.scheme.substr(4) : selector.uri.scheme;
    
            // strip first character
            selector.uri.path = selector.uri.path.substr(1);
    
            // strip slash at tail
            selector.uri.path = selector.uri.path.lastIndexOf("/") == selector.uri.path.length - 1? selector.uri.path.substr(0, selector.uri.path.length - 2) : selector.uri.path;
    
            // remove .git in repo name
            selector.uri.path = selector.uri.path.lastIndexOf(".git") == selector.uri.path.length - 4? selector.uri.path.substr(0, selector.uri.path.length - 4) : selector.uri.path;
    
            // replace path with gitlab api path
            selector.uri.path = "/api/v4/projects/" + selector.uri.path.replace(/\//g, "%2F");
    
            // get options
            var options = {
                "token" : selector.options && selector.options.gitlab? selector.options.gitlab.token : null,
                "branch" : selector.options && selector.options.gitlab? selector.options.gitlab.branch : null,
                "enableCache" : true,
                "enableNamespace" : false
            };

            // clone headers for use
            var headers = clone(HTTP_HEADERS);

            // add token to http headers if set
            if (options.token) {
                headers["PRIVATE-TOKEN"] = glToken;
            }
    
            // prepare repository and user name
            var repoName =     selector.uri.path.substr(selector.uri.path.lastIndexOf("/", selector.uri.path.length - 2)).split("%2F");
            var userName =     repoName[0].substr(1);
                repoName =     repoName[1];
            var resource =     selector.resource;
            var ns =           "com.gitlab." + userName;
            var archiveURL;
            var semverFilter = "";
            var uri;
            var resolveOptions = {
                "strip"   : 1,
                "headers" : headers,
                "ns" : (options.enableNamespace? ns : null)
            };

            // resolve branch uri (branch requests are not saved to local cache)
            if (options.branch) {
                selector.uri.path = "/api/v4/projects/" + userName + "%2F" + repoName + "/repository/archive?sha=" + options.branch;
                return resolve(resolveOptions);
            }
    
            // change repository name in api url
            selector.uri.path = "/api/v4/projects/" + userName + "%2F" + repoName + "/repository/tags";
        
            // get the semver filter from the uri
            if (selector.uri.fragment) {
                if (selector.uri.fragment.substr(0,7) != "semver:") {
                    throw "Unsupported URI fragment '" + selector.uri.fragment + "'.";
                }
    
                semverFilter = repoName + "." + selector.uri.fragment.substr(7);
            }
    
            var tagErr;

            var id;
            var archiveURL;
            var gitLabTag;
            var cacheURI;
            var cacheStream;
            var cacheVolume;
            var repoStream;
            var uriTags = selector.uri;
    
            function readGitLabReleaseTagsStream(stream) {
                stream.headers = headers;
                return stream.readAsJSON();
            }
            function processGitLabReleaseTags(tags) {
                return new Promise(function(resolve, reject) {
                    if (!tags || tags.length == 0) {
                        throw new Error("Package '" + selector.package + "' does not have any tags in the GitLab repository.");
                    }
                    var versions = [];
                    var count = 0;
                    for (var r in tags) {
                        var tagName = tags[r].name;
                        if (tagName.substr(0,1) == "v") {
                            tagName = tagName.substr(1);
                        }
        
                        //TODO
                        // CHECK SEMANTIC VERSION -> VALID
        
                        versions[tagName] = tags[r];
                        count++;
                    }
                    if (count == 0) {
                        throw new Error("Package '" + selector.package + "' does not contain one or more valid tags in the GitLab repository.");
                    }
        
                    var tag = version.find(versions, "", selector.upgradable || version.UPGRADABLE_NONE);
    
                    resolve(tag);
                });
            }
            function findReleaseInUsingCache(tag) {
                return new Promise(function(resolve, reject) {
                    if (tag instanceof Error) {
                        tagErr = tag;
                        tag = null;
                    }
        
                    if (tag) {
                        gitLabTag = tag;
                        archiveURL = selector.parseURI("https://" + HOST_GITLAB + "/api/v4/projects/" + userName + "%2F" + repoName + "/repository/archive?sha=" + tag.name).toString();
                    }

                    if (typeof using === "undefined") {
                        return resolve();
                    }

                    resolve(define.cache.get((options.enableNamespace? ns + "." : "") + (semverFilter || repoName) + (resource? "/" + resource : "")));
                });
            }
            function findReleaseInLocalCache(cacheModule) {
                return new Promise(function(resolve, reject) {
                    if (cacheModule) {
                        return resolve(cacheModule);
                    }
        
                    if (options.enableCache) {
                        resolve(config.getVolume()
                            .then(getCachedFilesFromLocalConfigVolume)
                            .catch(processCachedFilesResults)
                            .then(processCachedFilesResults)
                            .catch(resolveURI));
                    }
                    else {
                        resolve(resolveURI(archiveURL));
                    }
                });
            }
            function getCachedFilesFromLocalConfigVolume(volume) {
                cacheVolume = volume;
                return cacheVolume.query(PATH_CACHE + userName + "/");
            }
            function processCachedFilesResults(uriList) {
                return new Promise(function(resolve, reject) {
                    if (uriList && uriList.code == "ENOENT") {
                        uriList = [];
                    }
                    else if (uriList && uriList.code) {
                        throw new Error("Cache disk error.", uriList);
                    }
    
                    var cache = {};
                    for (var u in uriList) {
                        if (uriList[u].path.lastIndexOf("/") != uriList[u].path.length - 1) {
                            var file = uriList[u].path.substr(uriList[u].path.lastIndexOf("/") + 1);
                            if (file.substr(0, repoName.length + 1) == repoName + ".") {
                                cache[file.substr(0,file.length - 4)] = uriList[u];
                            }
                        }
                    }
    
                    // get highest version from cache
                    var highestCache = version.find(cache, semverFilter);
    
                    if (!archiveURL) {
                        // resolve highest cache version
                        return resolve(resolveURI(highestCache));
                    }
                    else {
                        id = repoName + "." + (gitLabTag.name.indexOf("v") == 0? gitLabTag.name.substr(1) : gitLabTag.name);
                        var found;
                        for (var u in cache) {
                            if (u == id) {
                                found = u;
                                break;
                            }
                        }
                        if (found) {
                            // release version from GitLab is present in cache
                            return resolve(resolveURI(cache[found]));
                        }
                        else {
                            // download new uri and save to cache
                            return resolve(ioURI.open(archiveURL)
                                .then(getCacheURI)
                                .then(openCacheStreamForWriting)
                                .then(writeToCacheStream)
                                .catch(closeCacheStream)
                                .then(closeCacheStream)
                                .catch(closeRepositoryStream)
                                .then(closeRepositoryStream)
                                .catch(ignore)
                                .then(resolveCacheURIOrGitLabURI));
                        }
                    }
                });
            }
            function getCacheURI(stream) {
                repoStream = stream;
                return cacheVolume.getURI(PATH_CACHE + userName + "/" + id + "." + EXT_PKX);
                
            }
            function openCacheStreamForWriting(uri) {
                cacheURI = uri;
                return cacheURI.open(ioAccess.OVERWRITE, true);
            }
            function writeToCacheStream(stream) {
                cacheStream = stream;
                repoStream.headers = headers;
                return repoStream.copyTo(cacheStream);
            }
            function closeRepositoryStream(e) {
                if (e instanceof Error) {
                    tagErr = e;
                }
                return repoStream.close();
            }
            function closeCacheStream(e) {
                if (e instanceof Error) {
                    tagErr = e;
                }
                return cacheStream.close();
            }
            function resolveCacheURIOrGitLabURI() {
                if (tagErr) {
                    // an error occurred while downloading the tarball (could be CORS), fallback to highest cached version.
                    return resolveURI(highestCache);
                }
                return resolveURI(cacheURI);
            }
            function resolveURI(uri) {
                return new Promise(function(resolve, reject) {
                    if (uri && uri.name) {
                        reject(new Error("An error occured while trying to fetch '" + selector.package + "' from the GitLab repository."));
                        return;
                    }
                    else if (!uri && !tagErr) {
                        reject(new Error("Couldn't find any suitable tag for package '" + selector.package + "' in the GitLab repository."));
                        return;
                    }
                    else if (!uri && tagErr) {
                        //if (!tag) {
                            reject(new Error("Downloading of package '" + selector.package + "' from GitLab failed. If you are running this in a browser, CORS might be the problem."));
                        //}
                        return;
                    }
                    try {
                        selector.uri = uri;
                        selector.id = (options.enableNamespace? ns + "." : "") + id + "/" + (resource? resource : "");
                        resolve(resolveOptions);
                    }
                    catch (e) {
                        reject(e);
                    }
                });
            }
            function ignore(e) {
                if (e instanceof Error) {
                    tagErr = e;
                }
            }

            resolve(uriTags.open()
                    .then(readGitLabReleaseTagsStream)
                    .then(processGitLabReleaseTags)
                    .catch(ignore)
                    .then(findReleaseInUsingCache)
                    .then(findReleaseInLocalCache)
                    .catch(ignore));
        });
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    //
    // Register Request Processor
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    define.Loader.waitFor("pkx", function(loader) {
        loader.addRequestProcessor(REQUEST_PROC_NAME, request);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////// 
    //
    // Register pix CLI Options
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    pix.on(pix.EVENT_PRE_PARSE_PARAMETERS, function (proc) {
        proc.option("--gl-token <token>",               "Sets the GitLab private token for requesting private repositories.");
        proc.option("--gl-branch <branch>",             "If set, the gitlab request will fetch the branch version (the last commit).");
    });
    pix.on(pix.EVENT_POST_PARSE_PARAMETERS, function (p, options) {
        options.gitlab = options.gitlab || {};
        if (p["--gl-token"]) {
            options.gitlab.token = p["--gl-token"].token;
        }
        if (p["--gl-branch"]) {
            options.gitlab.branch = p["--gl-branch"].branch;
        }
        // if (p["--gh-enable-pre-release"]) {
        //     var val = p["--gh-enable-pre-release"].enable;
        //     options.github = options.github || {};
        //     options.github.enablePreRelease = !(val == "false" || !val);
        // }
    });
    
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    module.exports = request;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://pix-request-gitlab.1.0.0/pix-request-gitlab.js
/////////////////////////////////////////////////////////////////////////////////////
//
// module 'buffer-subarray.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "buffer-subarray.1.0.0/";
    define.parameters.pkx = {
        "name": "buffer-subarray",
        "version": "1.0.0",
        "main": "buffer-subarray.js",
        "pkx": {
            "main": "buffer-subarray.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // buffer-subarray
    //
    //    Adds subarray function to Buffer's prototype if it does not exist.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Polyfill
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    if (typeof Buffer != "undefined" && typeof Buffer.prototype.subarray == "undefined") {
        Buffer.prototype.subarray = function (begin, end) {
            return this.slice(begin, end);
        };
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = typeof Buffer !== "undefined"? Buffer : {};
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://buffer-subarray.1.0.0/buffer-subarray.js

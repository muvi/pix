/////////////////////////////////////////////////////////////////////////////////////
//
// module 'config.1.0.2/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "config.1.0.2/";
    define.parameters.pkx = {
        "name": "config",
        "version": "1.0.2",
        "main": "config.js",
        "dependencies": {
            "error": "https://gitlab.com/muvi/error#semver:^1.0",
            "host": "https://gitlab.com/muvi/host#semver:^1.0",
            "event": "https://gitlab.com/muvi/event#semver:^1.0",
            "io": "https://gitlab.com/muvi/io#semver:^1.0",
            "io-access": "https://gitlab.com/muvi/io-access#semver:^1.0",
            "io-volume": "https://gitlab.com/muvi/io-volume#semver:^1.0",
            "io-driver-filesystem": "https://gitlab.com/muvi/io-driver-filesystem#semver:^1.0",
            "io-driver-localstorage": "https://gitlab.com/muvi/io-driver-localstorage#semver:^1.0"
        },
        "pkx": {
            "dependencies": {
                "error": "https://gitlab.com/muvi/error#semver:^1.0",
                "host": "https://gitlab.com/muvi/host#semver:^1.0",
                "event": "https://gitlab.com/muvi/event#semver:^1.0",
                "io": "https://gitlab.com/muvi/io#semver:^1.0",
                "io-access": "https://gitlab.com/muvi/io-access#semver:^1.0",
                "io-volume": "https://gitlab.com/muvi/io-volume#semver:^1.0",
                "io-driver-filesystem": "https://gitlab.com/muvi/io-driver-filesystem#semver:^1.0",
                "io-driver-localstorage": "https://gitlab.com/muvi/io-driver-localstorage#semver:^1.0"
            },
            "main": "config.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.parameters.dependencies.push(define.cache.get("error.1.0/"));
    define.parameters.dependencies.push(define.cache.get("host.1.0/"));
    define.parameters.dependencies.push(define.cache.get("event.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-access.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-volume.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-driver-filesystem.1.0/"));
    define.parameters.dependencies.push(define.cache.get("io-driver-localstorage.1.0/"));
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // config
    //
    //    Library for reading and writing config files.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var Error =    require("error");
    var host =     require("host");
    var event =    require("event");
    var io =       require("io");
    var ioAccess = require("io-access");
    var ioVolume = require("io-volume");
    var fs =       require("io-driver-filesystem");
    var ls =       require("io-driver-localstorage");
    
    var config;
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Config Class
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function Config() {
        var self = this;
    
        //
        // constants
        //
        var PATH_CONFIG_DEVICE_LINUX = "/etc/opt/";
        var PATH_CONFIG_DEVICE_WINDOWS = typeof process != "undefined"? process.env.ProgramData + "\\" : null;
        var PATH_CONFIG_DEVICE_MACOS = "/Library/Preferences/";
        var PATH_CONFIG_USER_LINUX = typeof process != "undefined"? process.env.HOME + "/.config/" : null;
        var PATH_CONFIG_USER_WINDOWS = typeof process != "undefined"? process.env.APPDATA + "\\" : null;
        var PATH_CONFIG_USER_MACOS = typeof process != "undefined"? process.env.HOME + "/Library/Preferences/" : null;
        this.MAX_SIZE = ls && ls.MAX_SIZE? ls.MAX_SIZE : "5242880";
        this.VOLUME_ID_LOCAL = "config-local";
    
        //
        // private
        //
        var volume;
        var ConfigurationVolume = function(mod, root) {
            this.id = self.VOLUME_ID_LOCAL;
    
            this.name = "Configuration (Local)";
            this.protocol = null;
            this.description = "Contains app configuration data.";
            this.size = self.MAX_SIZE;
            this.readOnly = false;

            this.getURI = function(path) {
                return mod.getURI(root + (path.indexOf("/") == 0? path.substr(1) : path));
            }
    
            this.open = function(path, opt_access, create_path) {
                return mod.open(root + (path.indexOf("/") == 0? path.substr(1) : path), opt_access, create_path);
            };

            this.delete = function(path) {
                return mod.delete(root + (path.indexOf("/") == 0? path.substr(1) : path));
            };
    
            this.exists = function(path) {
                return mod.exists(root + (path.indexOf("/") == 0? path.substr(1) : path));
            };
    
            this.query = function(path) {
                return mod.query(root + (path.indexOf("/") == 0? path.substr(1) : path));
            };
    
            this.events = new event.Emitter(this);
        };
        ConfigurationVolume.prototype = ioVolume;
    
        function mountConfigVolume(resolve, reject) {
            function registerAndResolve() {
                io.volumes.register(volume);
                resolve();
            }
            //mount config volume if not already mounted
            if (!volume) {
                return tryFileSystem().then(registerAndResolve).catch(function(e) {
                    if (e && e.name != host.ERROR_RUNTIME_NOT_SUPPORTED) {
                        reject(e);
                    }
                    return tryLocalStorage().then(registerAndResolve).catch(function(e) {
                        reject(e || new Error(host.ERROR_RUNTIME_NOT_SUPPORTED, "The runtime does not support saving local configuration."));
                    })
                });
            }
            else {
                resolve();
            }
        }
        function tryFileSystem() {
            return new Promise(function(resolve, reject) {
                var path;
                switch(host.platform) {
                    case host.PLATFORM_MACOS:
                        path = PATH_CONFIG_USER_MACOS;
                        break;
                    case host.PLATFORM_WINDOWS:
                        path = "/" + PATH_CONFIG_USER_WINDOWS;
                        break;
                }
                if (host.isPlatformLinuxFamily()) {
                    path = PATH_CONFIG_USER_LINUX;
                }
                return fs.getVolume(path && path.indexOf(":") > -1? path.substr(0, path.indexOf(":") + 1) : "").then(function(fsVolume) {
                    volume = new ConfigurationVolume(fsVolume, path && path.indexOf(":") > -1? "/" + path.substr(path.indexOf(":") + 2) : path);
                    resolve();
                }).catch(reject);
                // return fs.exists(path).then(function() {
                //     volume = new ConfigurationVolume(fs, path);
                //     resolve();
                // }).catch(reject);
            });
        }
        function tryLocalStorage() {
            return new Promise(function(resolve, reject) {
                return ls.getVolume().then(function(lsVolume) {
                    volume = new ConfigurationVolume(lsVolume, "/");
                    resolve();
                }).catch(reject);
                // return ls.exists("ls:///").then(function() {
                //     volume = new ConfigurationVolume(ls, "ls:///");
                //     resolve();
                // }).catch(reject);
            });
        }
    
        //
        // public
        //
        this.load = function(path) {
            return new Promise(function(resolve, reject) {
                function success() {
                    // load file from volume (if not exist, return blanco object)
                    return volume.open(path, ioAccess.READ).then(function(stream) {
                        return stream.readAsJSON().then(function(obj) {
                            return stream.close()
                            .then(function() {
                                resolve(obj);
                            })
                            .catch(reject);
                        }).catch(function(e) {
                            return stream.close()
                            .then(function() {
                                reject(e);
                            })
                            .catch(reject);
                        });
                    }).catch(reject);
                }
    
                if (!volume) {
                    mountConfigVolume(success, reject);
                }
                else {
                    success();
                }
            });
        };
    
        this.save = function(obj, path) {
            return new Promise(function(resolve, reject) {
                function success() {
                    // save file to volume (and create folders)
                    return volume.open(path, ioAccess.OVERWRITE, true).then(function(stream) {
                        var data = JSON.stringify(obj);
                        if (data.length > self.MAX_SIZE) {
                            reject(new Error(io.ERROR_FILE_SIZE_EXEEDS_LIMIT, "The configuration file is too big. There is a size limit of " + self.MAX_SIZE + " bytes per file for storing local configuration data."));
                        }
                        else {
                            return stream.write(data).then(function() {
                                return stream.close()
                                .then(function() {
                                    resolve();
                                })
                                .catch(reject);
                            }).catch(function(e) {
                                return stream.close()
                                .then(function() {
                                    reject(e);
                                })
                                .catch(reject);
                            });
                        }
                    }).catch(reject);
                }
    
                if (!path) {
                    reject(new Error(self.ERROR_INVALID_PATH, "There is no path specified to save the config."));
                }
                else if (!volume) {
                    mountConfigVolume(success, reject);
                }
                else {
                    success();
                }
            });
        };
    
        this.getVolume = function() {
            return new Promise(function(resolve, reject) {
                function success() {
                    resolve(volume);
                }
    
                if (!volume) {
                    mountConfigVolume(success, reject);
                }
                else {
                    success();
                }
            });
        };
    }
    Config.prototype.ERROR_INVALID_PATH = "Invalid Path";
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Singleton Instance
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    config = new Config();
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = config;
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://config.1.0.2/config.js

/////////////////////////////////////////////////////////////////////////////////////
//
// module 'io-access.1.0.0/'
//
/////////////////////////////////////////////////////////////////////////////////////
(function(module, using, require) {
    define.parameters = {};
    define.parameters.wrapped = true;
    define.parameters.system = "pkx";
    define.parameters.id = "io-access.1.0.0/";
    define.parameters.pkx = {
        "name": "io-access",
        "version": "1.0.0",
        "main": "io-access.js",
        "pkx": {
            "main": "io-access.js"
        }
    };
    define.parameters.dependencies = [ "pkx", "module", "configuration", "requirer" ];
    define.parameters.dependencies[0] = define.parameters.pkx;
    define.prepare();
    
    using = define.getUsing(define.parameters.id);
    require = define.getRequire(define.parameters.id, require);
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // io-access
    //
    //    Contains io access constants.
    //
    // License
    //    Apache License Version 2.0
    //
    // Copyright Nick Verlinden
    //
    ///////////////////////////////////////////////////////////////////////////////////////////// 
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    module.exports = {
        "READ"   : "read",
        "OVERWRITE"  : "overwrite",
        "MODIFY" : "modify",
        "ERROR_ACCESS_DENIED" : "Access Denied"
    };
    
    if(module.exports) {
        define(function factory() { return module.exports; });
    }
})({},typeof using != "undefined"? using : null, typeof require != "undefined"? require : null);
//# sourceURL=http://io-access.1.0.0/io-access.js

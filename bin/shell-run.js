#! /usr/bin/env node

// subscribe to unhandled promise event to show stacktrace
process.on("unhandledRejection", console.error);

// start pix
var pix = require("../pix.js");
pix().catch(console.error);
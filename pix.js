/////////////////////////////////////////////////////////////////////////////////////////////
//
// pix
//
//    Cross-runtime javascript bootloader.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden
//
/////////////////////////////////////////////////////////////////////////////////////////////
//
// require Function Polyfill
//
/////////////////////////////////////////////////////////////////////////////////////////////
if (typeof require === "undefined" || typeof document !== "undefined") {
    var original = typeof require !== "undefined"? (require.original || require) : null;

    require = function(source) {
        if (!require.pix && require.original) {
            return require.original.apply(require.original, arguments);
        }

        if (typeof source !== "string") {
            throw "Invalid request. Parameter 'source' should be of type 'string', but type '" + (typeof source) + "' was used.";
        }
        if (source.indexOf("./") !== 0 && source.indexOf("../") !== 0) {
            source = "./" + source;
        }
        if ((source.indexOf("./") !== 0 && source.indexOf("../") !== 0) || (source.lastIndexOf(".js") !== source.length - 3)) {
            var e = new Error("MODULE_NOT_FOUND", "Invalid request. Can't load modules at this stage, only script files.");
            e.code = "MODULE_NOT_FOUND";
            throw e;
        }

        if (typeof document !== "undefined") {
            document.write("<script language=\"javascript\" type=\"text/javascript\" src=\"" + source + "\"></script>");
            return;
        }

        throw "Unsupported runtime. Can't load script.";
    }
    require.pix = true;
    require.original = original;
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Scope Globals
//
/////////////////////////////////////////////////////////////////////////////////////////////
var pix;

(function() {
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Constants
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var CONFIG_DEFAULT = {
        "pix" : true
    };
    var PKX_DEFAULT_URI =             "https://gitlab.com/muvi/";
    var MSG_MISSING_FEATURE =         "This feature is not yet implemented.";
    var MSG_UI_UNAVAILABLE =          "UI Runtime is unavailable on this host.";
    var MSG_DEBUG_UNAVAILABLE =       "Debugging is only available in node.js runtime.";
    var MSG_THEME_UNAVAILABLE =       "Theming support is unavailable on this host.";
    var MODULE_ID_URI =               "io-uri.1";
    var MODULE_ID_CONFIG =            "config.1";
    var MODULE_ID_CLI =               "cli.1";
    var MODULE_ID_EVENT =             "event.1";
    var MODULE_ID_HOST =              "host.1";
    var MODULE_ID_CLONE =             "object-clone.1";
    var MODULE_ID_MERGE =             "object-merge.1";
    var PATH_CONFIG =                 "pix/pix.json";
    var ERROR_UNKNOWN =               "Unknown Error";
    var ERROR_DEPENDENCY =            "Dependency Error";
    var ERROR_INVALID_PROFILE =       "Invalid Profile";
    var ERROR_INVALID_SECTION =       "Invalid Section";
    var ERROR_SAVE_CONFIG =           "Save Config Failed";
    var ERROR_INVALID_PARAMETER =     "Invalid Parameter";
    var EVENT_PRE_PARSE_PARAMETERS =  "pix-pre-parse-parameters";
    var EVENT_POST_PARSE_PARAMETERS = "pix-post-parse-parameters";

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Privates
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    var ready;
    var config;
    var options;
    
    var ui;

    // dependencies
    var ioURI;
    var cfg;
    var cli;
    var clone;
    var event;
    var host;
    var io;
    var merge;
    var childProcess;
    var path;

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // getUrlParameters
    //
    // Returns url parameters if running inside browser, else returns an array containing one 
    // string; which is the name of the command invoked 'pix' to be compatible with CLI 
    // runtimes.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function getUrlParameters() {
        var params = [ "pix" ];
        if (typeof window === "undefined") {
            return;
        }
        window.location.search.substr(1).split("&").forEach(function (part) {
            if (!part) return;
            var item = part.split("=");
            for (var s in item) {
                params.push(decodeURIComponent(item[s]));
            }
        });
        return params;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // getNWJSParameters
    //
    // Returns cli parameters when running from inside nw.js, else returns null.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function getNWJSParameters() {
        try {
            return [ "pix" ].concat(require("nw.gui").App.argv);
        }
        catch(e) {
            return null;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // getNodeParameters
    //
    // Returns cli parameters when running from inside node.js, else returns null.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function getNodeParameters() {
        if (typeof process !== "undefined" && process.argv && process.argv.length > 1) {
            var params = process.argv.slice(1);
            params[0] = "pix";
            return params;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // parseParameters
    //
    // Returns an options object from the given cli parameters array.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function parseParameters(parameters, options) {
        options = options || {
            "selector" : null
        };

        var proc = new cli();

        if (host.runtime == host.RUNTIME_NODEJS) {
            proc.option("--debug [port]", "Starts node.js in debug mode.");
        }
        else {
            proc.option("--debug [port]", MSG_DEBUG_UNAVAILABLE);
        }
        if (host.isRuntimeBrowserFamily()) {
            proc.option("--theme <url>", "Loads the specified css theme.");
        }
        else {
            proc.option("--theme <url>", MSG_THEME_UNAVAILABLE);
        }
        proc.option("--config <json>", "A JSON object with parameters for the package module loaded.");
        if (host.runtime == host.RUNTIME_NODEJS) {
            proc.option("--version", "Displays the version of pix.");
        }
        proc.parameter("pix <selector>");

        pix.events.fire(EVENT_PRE_PARSE_PARAMETERS, proc);

        var p = proc.parse(parameters);

        pix.events.fire(EVENT_POST_PARSE_PARAMETERS, p, options);

        if (p) {
            if (p["--version"]) {
                options.version = p["--version"];
            }
            if (p["--debug"]) {
                options.debug = p["--debug"];
            }
            if (p["--config"]) {
                options.config = p["--config"];
            }
            if (p["--theme"] && typeof document !== "undefined") {
                options.theme = p["--theme"];
            }
            if (p["--help"]) {
                options.help = p["--help"];
            }
            else if (p.selector || p["--ui"]) {
                options.selector = p.selector;
            }
        }
        else {
            throw new Error(ERROR_INVALID_PARAMETER, "Parameters where missing or invalid. Please check the javascript console.");
        }

        return options;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // initEmbeddedDependencies
    //
    // Initialize embedded dependencies.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function initEmbeddedDependencies() {
        // get some embedded dependencies directly from the cache if they are not loaded already
        ioURI = ioURI || define.cache.get(MODULE_ID_URI, "minor").factory();
        cfg =   cfg ||   define.cache.get(MODULE_ID_CONFIG, "minor").factory();
        cli =   cli ||   define.cache.get(MODULE_ID_CLI, "minor").factory();
        event = event || define.cache.get(MODULE_ID_EVENT, "minor").factory();
        host =  host ||  define.cache.get(MODULE_ID_HOST, "minor").factory();
        clone = clone || define.cache.get(MODULE_ID_CLONE, "minor").factory();
        merge = merge || define.cache.get(MODULE_ID_MERGE, "minor").factory();

        pix.events = pix.events || new event.Emitter(pix);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // initAppOpenEvent
    //
    // Initialize nw.js app open event listener.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function initAppOpenEvent() {
        try {
            ui = require("nw.gui");
        }
        catch(e) {
            // ignore
        }

        if (ui) {
            // listen for OS open file event
            ui.App.on("open", function(cmdline) {
                cmdline = cmdline.replace(/"([^"]+)"/g, function(a) {
                    return a.replace(/\s/g, "&nbsp;");
                }).split(" ");
                for (var i = 0, length = cmdline.length, arg = "", args = []; i < length; ++i) {
                    arg = cmdline[i].replace(/&nbsp;/g, " ");
                    // filter by exe file and exe args.
                    if (arg === "\"" + process.execPath + "\"" || arg.search(/^\-\-/) === 0) continue;
                    args.push(arg);
                }
                pix(args[args.length -1]);
            });
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Init
    //
    // Initializes dependencies and configuration for first use.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function init(callback) {
        // now that the dependencies are loaded, we can switch to original require function
        if (require.pix) {
            require.pix = false;
        }

        initEmbeddedDependencies();
        initAppOpenEvent();
        
        function configSuccess(c) {
            config = c;
            configDone();
        }

        function configFail(e) {
            if (e && e.name != ioURI.ERROR_NO_ENTRY && e.name != host.ERROR_RUNTIME_NOT_SUPPORTED) {
                console.error(e);
            }
            configDone();
        }

        function configDone() {
            // if empty object, set to default
            if (!config || Object.keys(config).length === 0 || !config.pix) {
                config = CONFIG_DEFAULT;

                // attempt to save default config
                cfg.save(config, PATH_CONFIG).then(function() {
                    callback();
                }, callback);
            }
            else {
                callback();
            }
        }

        // start loading the configuration
        cfg.load(PATH_CONFIG).then(function(c) {
            configSuccess(c);   
        }, configFail);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // getDeepestError
    //
    // Returns the deepest error object.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function getDeepestError(e) {
        if (e.innerError) {
            return getDeepestError(e.innerError);
        }
        if (e.name) {
            errName = e.name;
        }
        return e;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // getModuleInfo
    //
    // Returns the deepest error object.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function getModuleInfo(module, indent) {
        var err = "";

        if (!indent) {
            indent = "";
        }
        // display basic package info
        err += "\n" + indent + "Request '" + module.id + "':" + "\n";
        // pkx specific info
        if (module.parameters && module.parameters.pkx) {
            err += indent + "    Name   : " + module.parameters.pkx.name + "\n";
            err += indent + "    Version: " + module.parameters.pkx.version + "\n";
            if (module.parameters.pkx.description) {
                err += indent + "    Description:" + "\n";
                err += indent + "        " + module.parameters.pkx.description + "\n";
            }
        }

        // display dependencies
        var foundDependencies = false;
        for (var m in module.dependencies) {
            // skip named dependencies, pkx, module & configuration
            if (isNaN(m) || m <= 2) {
                continue;
            }
            if (!foundDependencies) {
                err += indent + "    Dependencies:" + "\n";
                foundDependencies = true;
            }
            err += indent + "        • " + module.dependencies[m].id + "\n";
        }

        return err;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // getHumanError
    //
    // Returns an error object describing what went wrong with the request for reporting.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function getHumanError(loader, indent) {
        var err = "";
        var errName = ERROR_UNKNOWN;
        
        if (!indent) {
            indent = "";
        }

        // something went wrong with the loader
        for (var e in loader.err) {
            var deepE = getDeepestError(loader.err[e]);
            err += indent + deepE + "\n";
            if (deepE.name) {
                errName = deepE.name;
            }
        }

        // something went wrong with the individual requests
        for (var r in loader.requests) {
            if (loader.requests[r].module) {
                err += getModuleInfo(loader.requests[r].module, indent);
            }
            for (var e in loader.requests[r].err) {
                if (loader.requests[r].err[e].name == pix.ERROR_DEPENDENCY) {
                    err += indent + "    Dependencies:" + "\n";
                    err += indent + "        One or more dependencies failed to load." + "\n";
                    err += getHumanError(loader.requests[r].err[e].data, indent + "        ").message;
                }
                else {
                    var deepE = getDeepestError(loader.requests[r].err[e]);
                    err += indent + (!loader.requests[r].module? loader.requests[r].request.package+ ":\n    " + indent : "") + deepE + "\n";
                }
            }
        }

        var e = new Error(err);
            e.name = errName;
        return e;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // launchDebug
    //
    // Will launch a node.js instance with the given options and debugging features enabled.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function launchDebug(options) {
        if (typeof process === "undefined") {
            throw "This host does not support enabling debugging features.";
        }

        // start node in debug mode
        childProcess = childProcess || require("child_process");

        var PATH_CWD = process.cwd();

        // splice out pix command
        process.argv.splice(0, 1);

        // find debug argument
        var debugIdx = -1;
        for (var a in process.argv) {
            if (process.argv[a] == "--debug") {
                debugIdx = a;
            }
            process.argv[a] = process.argv[a].replace(/"/g, "\"");
        }
        if (debugIdx >= 0) {
            process.argv.splice(debugIdx, 1);
        }
        if (!options.debug.port) {
            process.argv.splice(0, 0, "--debug-brk");
            //process.argv.splice(0, 0, "--inspect");
        }
        else {
            if (debugIdx >= 0) {
                process.argv.splice(debugIdx, 1);
            }
            process.argv.splice(0, 0, "--debug-brk=" + options.debug.port);
            //process.argv.splice(0, 0, "--inspect=" + options.debug.port);
        }
    

        var ls = childProcess.spawn("node", process.argv, {"cwd": PATH_CWD});

        ls.stdout.on("data", function(data) {
            console.log(data.toString().trim());
        });

        ls.stderr.on("data", function(data) {
            console.error(data.toString().trim());
        });

        return;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // request
    //
    // Will process an pix request.
    // It has some public properties that can be used:
    //   .require                     the original require function of the current runtime.
    //   .parameters                  the parameters passed to pix
    //
    // options
    //   .help                        Displays parameter help if set.
    //   .selector                    The package selector.
    //   .config
    //      .json                     This json object will be passed to the package(s) in the selector.
    //   .debug                       If set, enables debugmode in node.js.
    //      .port                     Optionally sets the beug port.
    //   .theme                       If set, the given css file is loaded for theming the boot screen.
    //      .url                      The URL to the css file.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function request() {
        // create options from existing config
        var options = clone(config);
        var args = arguments;

        return new Promise(function(resolve, reject) {
            var parsedArgs = false;

            if (args.length == 1) {
                if (typeof args[0] === "string") {
                    args[0] = { "selector" : args[0] };
                }

                if (typeof args[0] !== "object") {
                    throw new TypeError("Invalid parameter type. An object was expected but got '" + (typeof args[0]) + "' instead.");
                }

                // merge given options with existing
                options = merge(args[0], options);
            }
            else if (args.length > 1) {
                throw new RangeError("Invalid number of parameters. Only one argument is accepted.");
            }
            else {
                // get options from parameters array, and merge with existing
                parseParameters(getNWJSParameters() || getNodeParameters() || getUrlParameters(), options);

                parsedArgs = true;
            }

            if (options.theme && typeof document !== "undefined") {
                var theme = document.createElement("link");
                theme.rel = "stylesheet";
                theme.href = parsedArgs? options.theme.url : options.theme;
                document.head.appendChild(theme);
            }

            var runningNode = host.isRuntimeNodeFamily();

            if (options.help) {
                if (typeof document !== "undefined") {
                    var e = new Error(options.help);
                    e.name = "help";
                    throw e;
                }
                return;
            }
            else if (options.version) {
                if (host.runtime == host.RUNTIME_NODEJS) {
                    console.log(require("./package.json").version);
                }
                else {
                    var e = new Error("The 'version' option is only available on the node.js runtime.");
                    e.name = ERROR_INVALID_PARAMETER;
                    throw e;
                }
                return;
            }
            else if (options.selector || runningNode) {
                if (options.debug) {
                    return launchDebug(options); //TODO -> What happens with promise?
                }

                var isDefaultUrl = options.selector && options.selector !== "pix" && options.selector.indexOf(".") == -1 && options.selector.indexOf("/") == -1;

                // if running in supported environment, add file protocol syntax to path type selectors
                if (runningNode) {
                    var fsPath;
                    if (!options.selector) {
                        fsPath = process.cwd();

                        if (host.platform === host.PLATFORM_WINDOWS) {
                            fsPath = "/" + fsPath;
                        }
                    }
                    else if (options.selector.indexOf(":") === -1 && (options.selector.indexOf("/") !== -1 || options.selector.indexOf("\\") !== -1)) {
                        fsPath = options.selector;
                    }
                    
                    if (fsPath) {
                        // encode path uri characters
                        var fragments = fsPath.replace(/\\/g, "/").split("/");
                        options.selector = "";
                        for (var f in fragments) {
                            if (f == fragments.length - 1 && fragments[f] == "") {
                                continue;
                            }
                            options.selector += encodeURIComponent(fragments[f]) + "/";
                        }
                        options.selector = ioURI.parse(options.selector);
                    }
                }
                
                var requests = [];
                var request =  { "package" : (isDefaultUrl ? PKX_DEFAULT_URI : "") + options.selector };
                if (options.config) {
                    var c;
                    if (parsedArgs) {
                        c = options.config.json;
                    }
                    else {
                        c = options.config;
                    }
                    if (typeof c === "string") {
                        var json;
                        try {
                            json = JSON.parse(c);
                        }
                        catch(e) {
                            var e = new Error("Make sure the data you pass to the --config switch is valid JSON data.");
                            e.name = "Invalid Configuration";
                            throw e;
                        }
                        c = json;
                    }
                    else if (typeof c !== "object") {
                        var e = new Error("Make sure the data you pass to the --config switch is a valid JSON object.");
                        e.name = "Invalid Configuration";
                        throw e;
                    }

                    request.configuration = c;
                }
                request.options = options;
                requests.push(request);

                if (requests) {
                    using.apply(using, requests).then(function () {
                        resolve.apply(null, arguments);
                    }, function (loader) {
                        if (isDefaultUrl && loader.requests[0].err[0] && loader.requests[0].err[0].name == "Network Error" && loader.requests[0].err[0].message.indexOf("The server returned code '404'") == 0) {
                            var e = new Error("Package '" + options.selector + "' does not exist in the muvi repository.");
                            e.name = "Unknown Package";

                            reject(e);
                        }
                        else {
                            reject(getHumanError(loader));
                        }
                    });
                }
            }
            else {
                var e = new Error("The boot sequence can't start because no package was specified. If you are the developer of the app using pix, then please make sure you specify the package to load.");
                e.name = "Invalid Package";
                throw e;
            }
        });
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // pix
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    pix = function() {
        var parameters = arguments;

        if (!ready) {
            return new Promise(function(resolve, reject) {
                // wait until the PKX package loader is ready
                define.Loader.waitFor("pkx", function pkxReady() {
                    function process() {
                        resolve(request.apply(parameters));
                    }

                    if (!ready) {
                        ready = true;
                        init(process);
                        return;
                    }

                    process();
                });
            });
        }
        
        return request.apply(this, parameters);
    }
    pix.events = null;
    pix.EVENT_PRE_PARSE_PARAMETERS =  EVENT_PRE_PARSE_PARAMETERS;
    pix.EVENT_POST_PARSE_PARAMETERS = EVENT_POST_PARSE_PARAMETERS;
    pix.ERROR_DEPENDENCY =            ERROR_DEPENDENCY;
    pix.ERROR_INVALID_PROFILE =       ERROR_INVALID_PROFILE;
    pix.ERROR_INVALID_SECTION =       ERROR_INVALID_SECTION;
    pix.ERROR_SAVE_CONFIG =           ERROR_SAVE_CONFIG;
    pix.ERROR_INVALID_PARAMETER =     ERROR_INVALID_PARAMETER;

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // pix.on
    //
    // Dummy function that will initialize dependencies and then call the event emitter's
    // on function. This dummy exists because an embedded module such as pix-request-gitlab
    // can call this function to listen for CLI parsing events and that occurs before the first
    // request, which will initialise the embedded dependencies.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    pix.on = function() {
        // init dependencies when an event listener is added (else the dependencies will be
        // initialised on the first request).
        event = define.cache.get(MODULE_ID_EVENT, "minor").factory();
        pix.on = null;
        pix.events = pix.events || new event.Emitter(pix);

        // now that event dependency is initialised, call the emitter function
        pix.on.apply(pix, arguments);
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // post-initialisation
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    // add to global scope
    if (typeof global != "undefined") {
        global.pix = pix;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Load Dependencies
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    require("./lib/using/using.js");

    // load embedded libraries
    require("./include.js");

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Export
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    if (typeof module !== "undefined") {
        module.exports = pix;
    }
})();